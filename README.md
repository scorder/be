# ScOrder API

#### HTTP

###### General
- Svi zahtjevi na BE idu na /api

###### Autentikacija
- Koristi se bearer autorizacija
- pri loginu na BE šalje se username i password,
nakon čega server u odgovoru vrati token
- token je potrebno staviti u header svakog HTTP zahtjeva prema backendu (izuzev
metoda za koje nije potrebna autentikacija) i backend prema tome tokenu
prepoznaje o kojem se useru radi
- u slučaju pogrešnog emaila i passworda API vraća HTTP error 401 (unauthorized)
- za provjeru radi li token, path /api/user/current vraća informacije trenutnog korisnika.