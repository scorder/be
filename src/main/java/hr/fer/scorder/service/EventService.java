package hr.fer.scorder.service;

import hr.fer.scorder.model.Event;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EventService {

    Event findEventById(Long id);

    List<Event> findEventsByCaffe(Long cafeId);

    List<Event> findEventsByDate(LocalDate date);

    boolean isQuiz(Event event);

    Event addEvent(Event event);

    Optional<Event> findEventTodayByCafe(Long cafeId);

}
