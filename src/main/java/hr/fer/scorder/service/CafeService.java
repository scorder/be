package hr.fer.scorder.service;

import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.CafeCurrentRbr;

import java.util.List;

public interface CafeService {

    Cafe findCafe(Long id);

    Cafe findCafeByEmail(String email);

    List<Cafe> findNearbyCafes(Double locationX, Double locationY, Double deviationX, Double deviationY);

    Cafe findCafeByLocation(Double locationX, Double locationY);

    List<Cafe> findCafeByName(String name);

    Cafe addUpdateCafe(Cafe cafe);

    CafeCurrentRbr updateCafeCurrentRbr(Cafe cafe, Long rbr);

    CafeCurrentRbr createCafeCurrentRbr(Cafe cafe);

    CafeCurrentRbr findRbrByCafe_Id(Long cafeId);

}
