package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.SuggestedSongs;
import hr.fer.scorder.repository.SuggestedSongsRepository;
import hr.fer.scorder.service.SuggestedSongsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class SuggestedSongsServiceImpl implements SuggestedSongsService {

    final SuggestedSongsRepository suggestedSongsRepository;

    @Autowired
    public SuggestedSongsServiceImpl(final SuggestedSongsRepository suggestedSongsRepository) {
        this.suggestedSongsRepository = suggestedSongsRepository;
    }

    @Override
    public List<SuggestedSongs> findSuggestions(Long cafeId) {
        return suggestedSongsRepository.findAllByCafe_Id(cafeId);
    }

    @Override
    public SuggestedSongs addASuggestion(SuggestedSongs suggestedSongs) {
        return suggestedSongsRepository.save(suggestedSongs);
    }

    @Override
    public SuggestedSongs findSuggestionByCafe_IdAndSong_Id(Long cafeId, Long songId) {
        Optional<SuggestedSongs> optionalSuggestion = suggestedSongsRepository.findByCafe_IdAndSong_Id(cafeId, songId);
        if (optionalSuggestion.isEmpty()) {
            throw new EntityNotFoundException("Ne postoji prijedlog za pjesmu " + Long.toString(songId) + " za cafe " + Long.toString(cafeId));
        }
        return optionalSuggestion.get();
    }

    @Override
    public Void removeSuggestion(SuggestedSongs suggestion) {
        suggestedSongsRepository.delete(suggestion);
        return null;
    }

    @Override
    public List<SuggestedSongs> findAllSuggestions() {

        return suggestedSongsRepository.findAll();
    }


}
