package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.CafeCurrentRbr;
import hr.fer.scorder.repository.CafeCurrentRbrRepository;
import hr.fer.scorder.repository.CafeRepository;
import hr.fer.scorder.service.CafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class CafeServiceImpl implements CafeService {

    private final CafeRepository cafeRepository;

    private final CafeCurrentRbrRepository cafeCurrentRbrRepository;

    @Autowired
    public CafeServiceImpl(final CafeRepository cafeRepository, final CafeCurrentRbrRepository cafeCurrentRbrRepository) {
        this.cafeRepository = cafeRepository;
        this.cafeCurrentRbrRepository = cafeCurrentRbrRepository;
    }

    @Override
    public Cafe findCafe(Long id) {
        final Optional<Cafe> cafeOptional = cafeRepository.findById(id);
        if (cafeOptional.isEmpty()) {
            throw new EntityNotFoundException("Cafe with id " + id + " not found.");
        }
        return cafeOptional.get();
    }

    @Override
    public List<Cafe> findNearbyCafes(Double locationX, Double locationY, Double deviationX, Double deviationY) {
        Double upperX = locationX + deviationX;
        Double lowerX = locationX - deviationX;
        Double upperY = locationY + deviationY;
        Double lowerY = locationY - deviationY;

        List<Cafe> listOfCafes = cafeRepository.findAllByLocationXBetweenAndLocationYBetween(lowerX, upperX, lowerY,
                upperY);
        if (listOfCafes.isEmpty()) {
            //throw new EntityNotFoundException("There are no cafes nearby.");
        }

        return listOfCafes;
    }

    @Override
    public Cafe findCafeByLocation(Double locationX, Double locationY) {
        final Optional<Cafe> cafeOptional = cafeRepository.findByLocationXAndLocationY(locationX, locationY);

        if (cafeOptional.isEmpty()) {
            throw new EntityNotFoundException(
                    "Cafes located on x:" + locationX.toString() + ", y:" + locationY.toString() + " not found.");
        }
        return cafeOptional.get();
    }

    @Override
    public List<Cafe> findCafeByName(String name) {
        List<Cafe> cafes = cafeRepository.findAllByName(name);
        if (cafes.isEmpty()) {
            throw new EntityNotFoundException("Cafe with name " + name + " not found.");
        }
        return cafes;
    }

    @Override
    public Cafe addUpdateCafe(Cafe cafe) {
        if (cafe.getId() == null) {
            Cafe addedCafe = cafeRepository.save(cafe);
            createCafeCurrentRbr(cafe);
            return addedCafe;
        }
        return cafeRepository.save(cafe);
    }

    @Override
    public Cafe findCafeByEmail(String email) {
        final Optional<Cafe> cafeOptional = cafeRepository.findByEmail(email);
        if (cafeOptional.isEmpty()) {
            throw new EntityNotFoundException("Cafe with the email " + email + " not found.");
        }
        return cafeOptional.get();
    }

    @Override
    public CafeCurrentRbr updateCafeCurrentRbr(Cafe cafe, Long rbr) {
        Optional<CafeCurrentRbr> cafeCurrentRbrOptional = cafeCurrentRbrRepository.findByCafe_Id(cafe.getId());
        if (cafeCurrentRbrOptional.isEmpty()) {
            throw new EntityNotFoundException("Cafe " + cafe.getName() + " doesn't have a rbr attached to it");
        }
        cafeCurrentRbrOptional.get().setRbr(rbr);
        return cafeCurrentRbrRepository.save(cafeCurrentRbrOptional.get());
    }

    @Override
    public CafeCurrentRbr createCafeCurrentRbr(Cafe cafe) {
        CafeCurrentRbr cafeCurrentRbr = new CafeCurrentRbr(null, cafe, (long) 1);
        return cafeCurrentRbrRepository.save(cafeCurrentRbr);
    }

    @Override
    public CafeCurrentRbr findRbrByCafe_Id(Long cafeId) {
        Optional<CafeCurrentRbr> cafeCurrentRbr = cafeCurrentRbrRepository.findByCafe_Id(cafeId);
        if (cafeCurrentRbr.isEmpty()) {
            throw new EntityNotFoundException("Cafe with id " + cafeId + " doesn't have rbr.");
        }
        return cafeCurrentRbr.get();
    }
}
    
