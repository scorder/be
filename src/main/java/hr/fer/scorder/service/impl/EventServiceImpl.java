package hr.fer.scorder.service.impl;


import hr.fer.scorder.model.Event;
import hr.fer.scorder.repository.EventRepository;
import hr.fer.scorder.repository.QuestionRepository;
import hr.fer.scorder.service.EventService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    private final QuestionRepository questionRepository;

    public EventServiceImpl(final EventRepository eventRepository, final QuestionRepository questionRepository) {
        this.eventRepository = eventRepository;
        this.questionRepository = questionRepository;
    }

    @Override
    public Event findEventById(Long id) {
        final Optional<Event> eventOptional = eventRepository.findById(id);
        if (eventOptional.isEmpty()) {
            throw new EntityNotFoundException("No event with id " + id);
        }
        return eventOptional.get();
    }

    @Override
    @Transactional
    public List<Event> findEventsByCaffe(Long cafeId) {

        List<Event> allEvents = eventRepository.findAllByCafe_IdOrderByDateStart(cafeId);
        List<Event> upcomingEvents = new ArrayList<>();
        LocalDateTime myTime = LocalDateTime.now();
        for (Event event : allEvents) {
            if (myTime.isBefore(event.getEnding())) {
                upcomingEvents.add(event);
            } else {
                questionRepository.deleteAllByEvent(event);
                eventRepository.delete(event);
            }
        }

        return upcomingEvents;
    }

    @Override
    public List<Event> findEventsByDate(LocalDate date) {
        List<Event> events = eventRepository.findAllByDateStartBetween(date.atStartOfDay(), date.atTime(23, 59, 59));
        if (events.isEmpty()) {
            throw new EntityNotFoundException("No events on this date: " + date);
        }
        return events;
    }

    @Override
    public boolean isQuiz(Event event) {
        return questionRepository.existsByEvent_Id(event.getId());
    }

    @Override
    public Optional<Event> findEventTodayByCafe(Long cafeId) {
        LocalDate now = LocalDate.now();
        return eventRepository.findFirstByCafe_IdAndDateStartBetweenOrderByDateStartAsc(cafeId, now.atStartOfDay(), now.atTime(23, 59, 59));
    }

    @Override
    public Event addEvent(Event event) {
        return eventRepository.save(event);
    }
}
