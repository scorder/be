package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.Song;
import hr.fer.scorder.repository.SongRepository;
import hr.fer.scorder.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class SongServiceImpl implements SongService {

    private final SongRepository songRepository;

    @Autowired
    public SongServiceImpl(final SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    @Override
    public Song findSong(Long id) {
        final Optional<Song> songOptional = songRepository.findById(id);
        if (songOptional.isEmpty()) {
            throw new EntityNotFoundException("Song with id " + id + " not found.");
        }
        return songOptional.get();
    }

    @Override
    public List<Song> findSongsBySongTitle(String songTitle) {
        List<Song> listOfSongs = songRepository.findAllBySongTitle(songTitle);
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("Song with the title \"" + songTitle + "\" not found.");
        }
        return listOfSongs;
    }

    @Override
    public List<Song> findSongsBySongTitleAndArtistName(String songTitle, String artistName) {
        List<Song> listOfSongs = songRepository.findAllBySongTitleAndArtistName(songTitle, artistName);
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("Song with the title \"" + songTitle + "\" from the artist " + artistName + " not found.");
        }
        return listOfSongs;
    }

    @Override
    public Song findSongBySongTitleAndArtistNameAndAlbumTitle(String songTitle, String artistName, String albumTitle) {
        Optional<Song> song = songRepository.findBySongTitleAndArtistNameAndAlbumTitle(songTitle, artistName, albumTitle);
        if (song.isEmpty()) {
            throw new EntityNotFoundException("Song with the title \"" + songTitle + "\" from the artist " + artistName + " in the album " + albumTitle + " not found.");
        }
        return song.get();
    }

    @Override
    public Song addSong(Song song) {
        if (song.getSongTitle().isEmpty() || song.getArtistName().isEmpty() || song.getSongLink().isEmpty()) {

        }
        return songRepository.save(song);
    }

    @Override
    public List<Song> findSongByArtistName(String artistName) {
        List<Song> listOfSongs = songRepository.findAllByArtistName(artistName);
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("Songs of artist " + artistName + " not found.");
        }
        return listOfSongs;
    }

    @Override
    public List<Song> findSongByAlbumTitle(String albumTitle) {
        List<Song> listOfSongs = songRepository.findAllByAlbumTitle(albumTitle);
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("Songs in the album " + albumTitle + " not found");
        }
        return listOfSongs;
    }

    @Override
    public List<Song> findSongByArtistNameAndAlbumTitle(String artistName, String albumTitle) {
        List<Song> listOfSongs = songRepository.findAllByArtistNameAndAlbumTitle(artistName, albumTitle);
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("Songs in album " + albumTitle + " of artist " + artistName + " not found.");
        }
        return listOfSongs;
    }

    @Override
    public List<Song> findAllSongs() {
        List<Song> listOfSongs = songRepository.findAll();
        if (listOfSongs.isEmpty()) {
            //throw new EntityNotFoundException("There are no songs in the database.");
        }
        return listOfSongs;
    }

}
