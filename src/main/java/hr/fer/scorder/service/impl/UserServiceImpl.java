package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.User;
import hr.fer.scorder.repository.UserRepository;
import hr.fer.scorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findUser(final Long id) {
        final Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty()) {
            throw new EntityNotFoundException("User with id " + id + " not found.");
        }
        return userOptional.get();
    }

    @Override
    public User findUserByEmail(final String email) {
        final Optional<User> userOptional = userRepository.findByEmail(email);
        if (userOptional.isEmpty()) {
            throw new EntityNotFoundException("User with email " + email + " not found.");
        }
        return userOptional.get();
    }

    @Override
    public User addUpdateUser(final User user) {
        return userRepository.save(user);
    }
}
