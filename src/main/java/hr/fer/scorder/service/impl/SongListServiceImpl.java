package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.SongList;
import hr.fer.scorder.repository.SongListRepository;
import hr.fer.scorder.service.SongListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongListServiceImpl implements SongListService {

    private final SongListRepository songListRepository;

    @Autowired
    public SongListServiceImpl(final SongListRepository songListRepository) {
        this.songListRepository = songListRepository;
    }

    @Override
    public List<SongList> findSongListByCafe(final Long cafeId) {
        List<SongList> songList = songListRepository.findAllByCafe_Id(cafeId);
        if (songList.isEmpty()) {
        }
        return songList;
    }

    @Override
    public SongList addSongList(SongList songList) {
        return songListRepository.save(songList);
    }

    @Override
    public List<SongList> addAllSongLists(List<SongList> songLists) {
        return songListRepository.saveAll(songLists);
    }

    @Override
    public SongList findSongInCafesSongList(Long cafeId, Long songId) {
        SongList songListElement = songListRepository.findByCafe_IdAndSong_Id(cafeId, songId);
        return songListElement;
    }

    @Override
    public void deleteSongFromSongList(SongList songListElement) {
        songListRepository.delete(songListElement);
        return;
    }
}
