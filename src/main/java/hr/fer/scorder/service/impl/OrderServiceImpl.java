package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.Order;
import hr.fer.scorder.repository.OrderRepository;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final CafeService cafeService;

    @Autowired
    public OrderServiceImpl(final OrderRepository orderRepository,
                            final CafeService cafeService) {
        this.orderRepository = orderRepository;
        this.cafeService = cafeService;
    }

    @Override
    public Order findOrder(final Long orderId) {
        final Optional<Order> orderOptional = orderRepository.findById(orderId);
        if (orderOptional.isEmpty()) {
            throw new EntityNotFoundException("Order not found");
        }
        return orderOptional.get();
    }

    @Override
    public List<Order> findUnfulfilledOrders() {
        final String email = JwtUtils.getCurrentUserUsername().get();
        final Cafe currentCafe = cafeService.findCafeByEmail(email);
        return orderRepository.findAllByTable_Cafe_IdAndFulfilledIsFalse(currentCafe.getId());
    }

    @Override
    public Order addOrder(final Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order markFulfilled(final Long orderId) {
        final Order order = findOrder(orderId);
        order.setFulfilled(true);
        return orderRepository.save(order);
    }

}
