package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.Item;
import hr.fer.scorder.repository.ItemRepository;
import hr.fer.scorder.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemServiceImpl(final ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public Item addItem(final Item item) {
        return itemRepository.save(item);
    }

    @Override
    public Item findItem(final Long id) {
        final Optional<Item> item = itemRepository.findById(id);
        if (item.isEmpty()) {
            throw new EntityNotFoundException("Item with id " + id + " not found.");
        }
        return item.get();
    }

    @Override
    public List<Item> getPriceList(final Long cafeId) {
        return itemRepository.findAllByCafeId(cafeId);
    }
}
