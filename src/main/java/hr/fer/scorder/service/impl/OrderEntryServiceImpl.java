package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.OrderEntry;
import hr.fer.scorder.repository.OrderEntryRepository;
import hr.fer.scorder.service.OrderEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderEntryServiceImpl implements OrderEntryService {

    private final OrderEntryRepository orderEntryRepository;

    @Autowired
    public OrderEntryServiceImpl(OrderEntryRepository orderEntryRepository) {
        this.orderEntryRepository = orderEntryRepository;
    }

    @Override
    public List<OrderEntry> addOrderEntries(List<OrderEntry> orderEntries) {
        List<OrderEntry> list = new ArrayList<>();
        for (var entry : orderEntries) {
            list.add(orderEntryRepository.save(entry));
        }
        return list;
    }

    @Override
    public OrderEntry addOrderEntry(OrderEntry orderEntry) {
        return orderEntryRepository.save(orderEntry);
    }
}
