package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.TableModel;
import hr.fer.scorder.repository.TableRepository;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.TableService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TableServiceImpl implements TableService {

    private final TableRepository tableRepository;

    public TableServiceImpl(final TableRepository tableRepository) {
        this.tableRepository = tableRepository;
    }

    @Override
    public TableModel findTable(final Long id) {
        final Optional<TableModel> tableOptional = tableRepository.findById(id);
        if (tableOptional.isEmpty()) {
            throw new EntityNotFoundException("No table with id " + id);
        }
        return tableOptional.get();
    }

    @Override
    public List<TableModel> findTablesByCafe(final Long cafeId) {
        return tableRepository.findAllByCafe_IdOrderByTableNumber(cafeId);
    }

    @Override
    public TableModel addTable(final TableModel table) {
        return tableRepository.save(table);
    }

    @Override
    public List<TableModel> addAllTables(final List<TableModel> tableModelList) {
        return tableRepository.saveAll(tableModelList);
    }

    @Override
    public TableModel setOccupied(final Integer tableNumber, final boolean occupied) {
        final String email = JwtUtils.getCurrentUserUsername().get();
        final Optional<TableModel> tableOptional = tableRepository.findByCafe_EmailAndTableNumber(email, tableNumber);
        if (tableOptional.isEmpty()) {
            throw new EntityNotFoundException("No table number " + tableNumber);
        }
        final TableModel tableModel = tableOptional.get();
        tableModel.setOccupied(occupied);
        return tableRepository.save(tableModel);
    }

    @Override
    public TableModel setOccupied(final Long tableId, final boolean occupied) {
        final Optional<TableModel> tableOptional = tableRepository.findById(tableId);
        if (tableOptional.isEmpty()) {
            throw new EntityNotFoundException("No table with id " + tableId);
        }
        final TableModel tableModel = tableOptional.get();
        tableModel.setOccupied(occupied);
        return tableRepository.save(tableModel);
    }
}
