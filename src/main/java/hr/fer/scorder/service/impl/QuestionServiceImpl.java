package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.Question;
import hr.fer.scorder.model.QuestionResponse;
import hr.fer.scorder.model.User;
import hr.fer.scorder.repository.QuestionRepository;
import hr.fer.scorder.repository.QuestionResponseRepository;
import hr.fer.scorder.service.QuestionService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;

    private final QuestionResponseRepository questionResponseRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository,
                               final QuestionResponseRepository questionResponseRepository) {
        this.questionRepository = questionRepository;
        this.questionResponseRepository = questionResponseRepository;
    }

    @Override
    public Question findQuestionById(Long id) {
        final Optional<Question> questionOptional = questionRepository.findById(id);
        if (questionOptional.isEmpty()) {
            throw new EntityNotFoundException("No question with id " + id);
        }
        return questionOptional.get();
    }

    @Override
    public Question findQuestionByQuestionAndEvent(String question, Long eventId) {
        final Optional<Question> questionOptional = questionRepository.findByQuestionAndEvent_Id(question, eventId);
        if (questionOptional.isEmpty()) {
            throw new EntityNotFoundException("No matching question");
        }
        return questionOptional.get();
    }

    @Override
    public List<Question> findQuestionsByEvent(Long eventId) {
        return questionRepository.findAllByEvent_IdOrderByQuestionNumberAsc(eventId);
    }

    @Override
    public List<Map.Entry<String, Double>> getPercentagesCorrectByUsername(Long eventId) {
        LinkedHashMap<String, Double> resultMap = new LinkedHashMap<>();

        final List<User> userList = questionResponseRepository.findAllUsersOnEvent(eventId);
        final Long totalQuestions = questionRepository.countAllByEvent_Id(eventId);
        for (User user : userList) {
            final Long correctQuestions = questionResponseRepository.countAllByQuestion_Event_IdAndUser_IdAndCorrectIsTrue(eventId, user.getId());
            double percentage = 0;
            if (totalQuestions!=0) {
                percentage = 100 * ((double) correctQuestions) / totalQuestions;
            }
            resultMap.put(user.getUsername(), percentage);
        }
        return resultMap.entrySet().stream().sorted(new CustomComparator().reversed()).collect(Collectors.toList());
    }

    @Override
    public QuestionResponse saveResponse(QuestionResponse questionResponse) {
        return questionResponseRepository.save(questionResponse);
    }

    @Override
    public boolean alreadyAnswered (Long questionId, Long userId) {
        return questionResponseRepository.existsByQuestion_IdAndUser_Id(questionId, userId);
    }

    @Override
    public Question addQuestion(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public List<Question> addQuestions(final List<Question> questions) {
        return questionRepository.saveAll(questions);
    }

    private static class CustomComparator implements Comparator<Map.Entry<String, Double>> {
        @Override
        public int compare(final Map.Entry<String, Double> o1, final Map.Entry<String, Double> o2) {
            return o1.getValue().compareTo(o2.getValue());
        }
    }
}
