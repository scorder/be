package hr.fer.scorder.service.impl;

import hr.fer.scorder.model.ActivePlaylist;
import hr.fer.scorder.repository.ActivePlaylistRepository;
import hr.fer.scorder.service.ActivePlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class ActivePlaylistServiceImpl implements ActivePlaylistService {

    private final ActivePlaylistRepository activePlaylistRepository;

    @Autowired
    public ActivePlaylistServiceImpl(final ActivePlaylistRepository activePlaylistRepository) {
        this.activePlaylistRepository = activePlaylistRepository;
    }

    @Override
    public List<ActivePlaylist> findPlaylist(Long cafeId, Long rbr) {
        List<ActivePlaylist> currentPlaylist = activePlaylistRepository.findAllByCafe_IdAndRbrGreaterThanOrderByRbr(cafeId, rbr);
        return currentPlaylist;
    }

    @Override
    public ActivePlaylist findCurrentSong(Long cafeId, Long rbr) {
        Optional<ActivePlaylist> currentSong = activePlaylistRepository.findByCafe_IdAndRbr(cafeId, rbr);
        if (currentSong.isEmpty()) {
            throw new EntityNotFoundException("There is no song playing.");
        }
        return currentSong.get();
    }

    @Override
    public ActivePlaylist addSongToPlaylist(ActivePlaylist activePlaylist) {
        try {
            ActivePlaylist addedElement = activePlaylistRepository.save(activePlaylist);
            return addedElement;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public List<ActivePlaylist> findActivePlaylists(Long cafeId) {
        return activePlaylistRepository.findByCafe_Id(cafeId);
    }


}
