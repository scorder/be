package hr.fer.scorder.service;

import hr.fer.scorder.model.User;

public interface UserService {

    User findUser(Long id);

    User findUserByEmail(String email);

    User addUpdateUser(User user);
}
