package hr.fer.scorder.service;

import hr.fer.scorder.model.ActivePlaylist;

import java.util.List;

public interface ActivePlaylistService {

    List<ActivePlaylist> findPlaylist(Long cafeId, Long rbr);

    ActivePlaylist findCurrentSong(Long cafeId, Long rbr);

    ActivePlaylist addSongToPlaylist(ActivePlaylist activePlaylist);

    List<ActivePlaylist> findActivePlaylists(Long cafeId);

}
