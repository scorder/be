package hr.fer.scorder.service;

import hr.fer.scorder.model.Song;

import java.util.List;

public interface SongService {

    Song findSong(Long id);

    List<Song> findAllSongs();

    List<Song> findSongsBySongTitle(String songTitle);

    List<Song> findSongsBySongTitleAndArtistName(String songTitle, String artistName);

    List<Song> findSongByArtistName(String artistName);

    List<Song> findSongByAlbumTitle(String albumTitle);

    List<Song> findSongByArtistNameAndAlbumTitle(String artistName, String albumTitle);

    Song findSongBySongTitleAndArtistNameAndAlbumTitle(String songTitle, String artistName, String albumTitle);

    Song addSong(Song song);
}
