package hr.fer.scorder.service;

import hr.fer.scorder.model.Question;
import hr.fer.scorder.model.QuestionResponse;

import java.util.List;
import java.util.Map;

public interface QuestionService {

    Question findQuestionById(Long id);

    Question findQuestionByQuestionAndEvent(String question, Long eventId);

    List<Question> findQuestionsByEvent(Long eventId);

    Question addQuestion(Question question);

    List<Question> addQuestions(List<Question> questions);

    boolean alreadyAnswered (Long questionId, Long userId);

    QuestionResponse saveResponse(QuestionResponse questionResponse);

    List<Map.Entry<String, Double>> getPercentagesCorrectByUsername(Long eventId);
}
