package hr.fer.scorder.service;

import hr.fer.scorder.model.Item;

import java.util.List;

public interface ItemService {

    Item addItem(Item item);

    Item findItem(Long id);

    List<Item> getPriceList(Long cafeId);

}
