package hr.fer.scorder.service;

import hr.fer.scorder.model.SuggestedSongs;

import java.util.List;

public interface SuggestedSongsService {

    List<SuggestedSongs> findSuggestions(Long cafeId);

    SuggestedSongs addASuggestion(SuggestedSongs suggestedSongs);

    SuggestedSongs findSuggestionByCafe_IdAndSong_Id(Long cafeId, Long songId);

    Void removeSuggestion(SuggestedSongs suggestion);

    List<SuggestedSongs> findAllSuggestions();
}
