package hr.fer.scorder.service;

import hr.fer.scorder.model.SongList;

import java.util.List;

public interface SongListService {

    List<SongList> findSongListByCafe(Long cafeId);

    SongList addSongList(SongList songList);

    List<SongList> addAllSongLists(List<SongList> songLists);

    SongList findSongInCafesSongList(Long cafeId, Long songId);

    void deleteSongFromSongList(SongList songListElement);
}
