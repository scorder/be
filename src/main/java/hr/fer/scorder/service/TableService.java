package hr.fer.scorder.service;

import hr.fer.scorder.model.TableModel;

import java.util.List;

public interface TableService {

    TableModel findTable(Long id);

    List<TableModel> findTablesByCafe(Long cafeId);

    TableModel addTable(TableModel table);

    List<TableModel> addAllTables(List<TableModel> tableModelList);

    TableModel setOccupied(Integer tableNumber, boolean occupied);

    TableModel setOccupied(Long tableId, boolean occupied);

}