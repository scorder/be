package hr.fer.scorder.service;

import hr.fer.scorder.model.OrderEntry;

import java.util.List;

public interface OrderEntryService {

    List<OrderEntry> addOrderEntries(List<OrderEntry> orderEntries);

    OrderEntry addOrderEntry(OrderEntry orderEntry);
}
