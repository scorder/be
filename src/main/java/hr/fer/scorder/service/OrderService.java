package hr.fer.scorder.service;

import hr.fer.scorder.model.Order;

import java.util.List;

public interface OrderService {

    Order findOrder(Long orderId);

    Order addOrder(Order order);

    List<Order> findUnfulfilledOrders();

    Order markFulfilled(Long orderId);

}
