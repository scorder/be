package hr.fer.scorder.repository;

import hr.fer.scorder.model.Question;
import hr.fer.scorder.model.QuestionResponse;
import hr.fer.scorder.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface QuestionResponseRepository extends JpaRepository<QuestionResponse, Long> {

    @Query("SELECT DISTINCT q.user FROM QuestionResponse q WHERE q.question.event.id=:eventId")
    List<User> findAllUsersOnEvent(Long eventId);

    Long countAllByQuestion_Event_IdAndUser_IdAndCorrectIsTrue(Long eventId, Long userID);

    boolean existsByQuestion_IdAndUser_Id(Long questionId, Long userId);

}
