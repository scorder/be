package hr.fer.scorder.repository;

import hr.fer.scorder.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findAllByCafe_IdOrderByDateStart(Long cafeId);

    List<Event> findAllByDateStartBetween(LocalDateTime beginning, LocalDateTime end);

    Optional<Event> findFirstByCafe_IdAndDateStartBetweenOrderByDateStartAsc(Long cafeId, LocalDateTime beginning, LocalDateTime end);

    Event findByEventName(String eventName);

}
