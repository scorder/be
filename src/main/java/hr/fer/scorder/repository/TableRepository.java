package hr.fer.scorder.repository;

import hr.fer.scorder.model.TableModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TableRepository extends JpaRepository<TableModel, Long> {

    List<TableModel> findAllByCafe_IdOrderByTableNumber(Long cafeId);

    Optional<TableModel> findByCafe_EmailAndTableNumber(String email, Integer tableNumber);
}
