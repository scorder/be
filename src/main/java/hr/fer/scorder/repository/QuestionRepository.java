package hr.fer.scorder.repository;

import hr.fer.scorder.model.Event;
import hr.fer.scorder.model.Question;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findAllByEvent_IdOrderByQuestionNumberAsc(Long eventId);

    Long countAllByEvent_Id(Long eventId);

    Optional<Question> findByQuestionAndEvent_Id(String question, Long eventId);

    List<Question> deleteAllByEvent(Event event);

    boolean existsByEvent_Id(Long eventId);
}
