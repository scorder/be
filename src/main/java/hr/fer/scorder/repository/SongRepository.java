package hr.fer.scorder.repository;

import hr.fer.scorder.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

    List<Song> findAllBySongTitle(String songTitle);

    List<Song> findAllByArtistName(String artistName);

    List<Song> findAllByAlbumTitle(String albumTitle);

    List<Song> findAllBySongTitleAndArtistName(String songTitle, String artistName);

    List<Song> findAllByArtistNameAndAlbumTitle(String artistName, String albumTitle);

    Optional<Song> findBySongTitleAndArtistNameAndAlbumTitle(String songTitle, String artistName, String albumTitle);


}
