package hr.fer.scorder.repository;

import hr.fer.scorder.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findAllByTable_Cafe_IdAndFulfilledIsFalse(final Long cafeId);
}
