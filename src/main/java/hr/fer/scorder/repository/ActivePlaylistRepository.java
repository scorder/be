package hr.fer.scorder.repository;

import hr.fer.scorder.model.ActivePlaylist;
import hr.fer.scorder.model.ActivePlaylistId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActivePlaylistRepository extends JpaRepository<ActivePlaylist, ActivePlaylistId> {

    List<ActivePlaylist> findAllByCafe_IdAndRbrGreaterThanOrderByRbr(Long cafeId, Long rbr);

    Optional<ActivePlaylist> findByCafe_IdAndRbr(Long cafeId, Long rbr);

    List<ActivePlaylist> findByCafe_Id(Long cafeId);

}
