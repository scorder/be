package hr.fer.scorder.repository;

import hr.fer.scorder.model.Cafe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CafeRepository extends JpaRepository<Cafe, Long> {

    Optional<Cafe> findByEmail(String email);

    List<Cafe> findAllByLocationXBetweenAndLocationYBetween(Double locationX1, Double locationX2, Double locationY1, Double locationY2);

    Optional<Cafe> findByLocationXAndLocationY(Double locationX, Double locationY);

    List<Cafe> findAllByName(String name);
}
