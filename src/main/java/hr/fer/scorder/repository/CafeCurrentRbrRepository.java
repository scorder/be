package hr.fer.scorder.repository;

import hr.fer.scorder.model.CafeCurrentRbr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CafeCurrentRbrRepository extends JpaRepository<CafeCurrentRbr, Long> {

    Optional<CafeCurrentRbr> findByCafe_Id(Long cafeId);

}
