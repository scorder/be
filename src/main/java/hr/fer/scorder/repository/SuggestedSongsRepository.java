package hr.fer.scorder.repository;

import hr.fer.scorder.model.SuggestedSongs;
import hr.fer.scorder.model.SuggestedSongsId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SuggestedSongsRepository extends JpaRepository<SuggestedSongs, SuggestedSongsId> {

    List<SuggestedSongs> findAllByCafe_Id(Long cafeId);

    Optional<SuggestedSongs> findByCafe_IdAndSong_Id(Long cafeId, Long songId);

}
