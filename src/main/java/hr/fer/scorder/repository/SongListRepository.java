package hr.fer.scorder.repository;

import hr.fer.scorder.model.SongList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SongListRepository extends JpaRepository<SongList, Long> {

    List<SongList> findAllByCafe_Id(Long cafeId);

    SongList findByCafe_IdAndSong_Id(Long cafeId, Long songId);

}
