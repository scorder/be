package hr.fer.scorder.security.jwt;

import hr.fer.scorder.security.CafeUserDetailsServiceImpl;
import hr.fer.scorder.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AuthTokenFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private CafeUserDetailsServiceImpl cafeUserDetailsService;

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {
        try {
            final String jwt = parseJwt(httpServletRequest);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                final String email = jwtUtils.getEmailFromToken(jwt);
                final List<String> authorities = jwtUtils.getAuthoritiesFromToken(jwt);
                final UserDetails userDetails;
                if (authorities.contains("cafe")) {
                    userDetails = cafeUserDetailsService.loadUserByUsername(email);
                } else {
                    userDetails = userDetailsService.loadUserByUsername(email);
                }
                List<GrantedAuthority> newAuthorities = new ArrayList<>(userDetails.getAuthorities());
                if (authorities.contains("owner")) {
                    newAuthorities.add((GrantedAuthority) () -> "owner");
                }
                final UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, newAuthorities);

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            logger.error("Can not set user autentication: {}", e);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String parseJwt(HttpServletRequest request) {
        final String headerAuth = request.getHeader(AUTHORIZATION_HEADER);
        final String TOKEN_TYPE = "Bearer ";
        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith(TOKEN_TYPE)) {
            final int BEGIN_INDEX = 7;
            return headerAuth.substring(BEGIN_INDEX);
        }
        return null;
    }
}
