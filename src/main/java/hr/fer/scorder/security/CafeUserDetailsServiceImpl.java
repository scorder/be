package hr.fer.scorder.security;

import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.service.CafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class CafeUserDetailsServiceImpl implements UserDetailsService {

    private final CafeService cafeService;

    @Autowired
    public CafeUserDetailsServiceImpl(final CafeService cafeService) {
        this.cafeService = cafeService;
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        try {
            final Cafe cafe;
            cafe = cafeService.findCafeByEmail(email);
            return UserDetailsImpl.build(cafe);
        } catch (EntityNotFoundException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}