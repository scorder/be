package hr.fer.scorder.security;

import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.User;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserCafeAuthenticationProvider implements AuthenticationProvider {

    private UserService userService;

    private CafeService cafeService;

    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        final String savedPassword;
        final String tokenUsername = auth.getName();
        final String tokenPassword = auth.getCredentials().toString();
        final List<String> authorities = auth.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        if (authorities.contains("cafe")) {
            Cafe cafe = cafeService.findCafeByEmail(tokenUsername);
            if (authorities.contains("owner")) {
                savedPassword = cafe.getPin();
            } else {
                savedPassword = cafe.getPassword();
            }
        } else if (authorities.contains("user")) {
            User user = userService.findUserByEmail(tokenUsername);
            savedPassword = user.getPassword();
        } else {
            throw new BadCredentialsException("External system authentication failed");
        }
        if (passwordEncoder.matches(tokenPassword, savedPassword)) {
            return auth;
        } else {
            throw new BadCredentialsException("External system authentication failed");
        }
    }

    @Autowired
    public void setUserService(final UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCafeService(final CafeService cafeService) {
        this.cafeService = cafeService;
    }

    @Autowired
    public void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}