package hr.fer.scorder.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class UserDetailsImpl implements UserDetails {

    private final Long id;

    private final String email;

    private final Collection<? extends GrantedAuthority> authorities;

    @JsonIgnore
    private final String password;

    public UserDetailsImpl(final Long id,
                           final String email,
                           final Collection<? extends GrantedAuthority> authorities,
                           final String password) {
        this.id = id;
        this.email = email;
        this.authorities = authorities;
        this.password = password;
    }

    public static UserDetailsImpl build(final User user) {
        return new UserDetailsImpl(user.getId(), user.getEmail(), List.of((GrantedAuthority) () -> "user"), user.getPassword());
    }

    public static UserDetailsImpl build(final Cafe cafe) {
        return new UserDetailsImpl(cafe.getId(), cafe.getEmail(), List.of((GrantedAuthority) () -> "cafe"), cafe.getPassword());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.copyOf(authorities);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
