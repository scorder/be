package hr.fer.scorder.model;

import java.io.Serializable;

public class ActivePlaylistId implements Serializable {

    private Long cafe;

    private Long rbr;

    public ActivePlaylistId() {
    }

    public ActivePlaylistId(Long cafe_id, Long rbr) {
        this.cafe = cafe_id;
        this.rbr = rbr;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ActivePlaylistId)) {
            return false;
        }
        ActivePlaylistId activePlaylistId = (ActivePlaylistId) o;

        boolean cafeEquals = (this.cafe == null && activePlaylistId.cafe == null)
                || this.cafe != null && this.cafe.equals(activePlaylistId.cafe);

        boolean rbrEquals = (this.rbr == null && activePlaylistId.rbr == null)
                || this.rbr != null && this.rbr.equals(activePlaylistId.rbr);

        return cafeEquals && rbrEquals;
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (cafe != null) {
            result = 31 * result + cafe.hashCode();
        }
        if (rbr != null) {
            result = 31 * result + rbr.hashCode();
        }
        return result;
    }

    public Long getCafe() {
        return cafe;
    }

    public void setCafe(Long cafe) {
        this.cafe = cafe;
    }

    public Long getRbr() {
        return rbr;
    }

    public void setRbr(Long rbr) {
        this.rbr = rbr;
    }
}
