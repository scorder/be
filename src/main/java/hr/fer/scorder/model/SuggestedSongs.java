package hr.fer.scorder.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(SuggestedSongsId.class)
@Table(name = "suggested_songs")
public class SuggestedSongs {
    @Id
    @ManyToOne
    @JoinColumn(name = "cafe_id")
    Cafe cafe;

    @Id
    @ManyToOne
    @JoinColumn(name = "song_id")
    Song song;

    public SuggestedSongs() {
    }

    public SuggestedSongs(Cafe cafe, Song song) {
        this.cafe = cafe;
        this.song = song;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }


}
