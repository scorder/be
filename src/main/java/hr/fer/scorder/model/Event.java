package hr.fer.scorder.model;


import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "cafe_id")
    private Cafe cafe;

    @Column(nullable = false, name = "event_name")
    private String eventName;

    @Column(nullable = false, name = "beginning_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime dateStart;

    @Column(nullable = false, name = "ending_date", columnDefinition = "TIMESTAMP")
    private LocalDateTime ending;

    @Column(nullable = false, name = "event_info")
    private String eventInfo;


    public Event() {
    }

    public Event(Long id, Cafe cafe, String eventName, LocalDateTime dateStart, LocalDateTime ending, String eventInfo) {
        this.id = id;
        this.cafe = cafe;
        this.eventName = eventName;
        this.dateStart = dateStart;
        this.ending = ending;
        this.eventInfo = eventInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDateTime date) {
        this.dateStart = date;
    }

    public LocalDateTime getEnding() {
        return ending;
    }

    public void setEnding(LocalDateTime ending) {
        this.ending = ending;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(String eventInfo) {
        this.eventInfo = eventInfo;
    }
}
