package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "cafe")
public class Cafe {

    @OneToMany(mappedBy = "cafe")
    Set<SongList> songList;
    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;
    @Column(nullable = false, unique = true, name = "email")
    private String email;
    @Column(nullable = false, name = "password")
    private String password;
    @Column(nullable = false, name = "pin")
    private String pin;
    @Column(nullable = false, name = "name")
    private String name;
    @Column(nullable = false, name = "location_x")
    private Double locationX;
    @Column(nullable = false, name = "location_y")
    private Double locationY;
    @Column(nullable = false, name = "date_of_registry")
    private LocalDate dateOfRegistry;

    public Cafe() {
    }

    public Cafe(Long id, String email, String password, final String pin, String name, final Double locationX, final Double locationY, final LocalDate dateOfRegistry) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.pin = pin;
        this.name = name;
        this.locationX = locationX;
        this.locationY = locationY;
        this.dateOfRegistry = dateOfRegistry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(final String pin) {
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(final Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(final Double locationY) {
        this.locationY = locationY;
    }

    public LocalDate getDateOfRegistry() {
        return dateOfRegistry;
    }

    public void setDateOfRegistry(final LocalDate dateOfRegistry) {
        this.dateOfRegistry = dateOfRegistry;
    }
}
