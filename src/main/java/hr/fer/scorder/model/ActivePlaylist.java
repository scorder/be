package hr.fer.scorder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(ActivePlaylistId.class)
@Table(name = "active_playlist")
public class ActivePlaylist {
    @Id
    @ManyToOne
    @JoinColumn(name = "cafe_id")
    Cafe cafe;

    @ManyToOne
    @JoinColumn(name = "song_id")
    Song song;

    @Id
    @Column(name = "rbr")
    private Long rbr;

    public ActivePlaylist() {
    }

    public ActivePlaylist(Cafe cafe, Song song, Long rbr) {
        this.cafe = cafe;
        this.song = song;
        this.rbr = rbr;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    public Long getRbr() {
        return rbr;
    }

    public void setRbr(Long rbr) {
        this.rbr = rbr;
    }
}
