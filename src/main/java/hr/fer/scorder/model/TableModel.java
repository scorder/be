package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tables")
public class TableModel {

    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "cafe_id")
    private Cafe cafe;

    @Column(name = "table_number")
    private Integer tableNumber;

    @Column(name = "occupied")
    private boolean occupied;

    public TableModel() {
    }

    public TableModel(final Long id, final Cafe cafe, final Integer tableNumber, final boolean occupied) {
        this.id = id;
        this.cafe = cafe;
        this.tableNumber = tableNumber;
        this.occupied = occupied;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(final Cafe cafe) {
        this.cafe = cafe;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(final Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(final boolean occupied) {
        this.occupied = occupied;
    }
}
