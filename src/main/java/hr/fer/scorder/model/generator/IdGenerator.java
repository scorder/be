package hr.fer.scorder.model.generator;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Random;

@Component
public class IdGenerator implements IdentifierGenerator {

    public static final String generatorName = "idGenerator";

    public Random randomGenerator;

    public IdGenerator() {
        this.randomGenerator = new Random();
    }

    @Override
    public Serializable generate(final SharedSessionContractImplementor sharedSessionContractImplementor, final Object o) throws HibernateException {
        return Integer.toUnsignedLong(randomGenerator.nextInt(16777216));
    }
}