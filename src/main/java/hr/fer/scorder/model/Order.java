package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "table_id")
    private TableModel table;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "order")
    private List<OrderEntry> orderEntries;

    @Column(nullable = false, name = "fulfilled")
    private boolean fulfilled;

    @Column(nullable = false, name = "order_timestamp", columnDefinition = "TIMESTAMP")
    private LocalDateTime orderTimestamp;

    public Order() {
    }

    public Order(final Long id, final TableModel table, final User user, final List<OrderEntry> orderEntries,
                 final boolean fulfilled, final LocalDateTime orderTimestamp) {
        this.id = id;
        this.table = table;
        this.user = user;
        this.orderEntries = List.copyOf(orderEntries);
        this.fulfilled = fulfilled;
        this.orderTimestamp = orderTimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public TableModel getTable() {
        return table;
    }

    public void setTable(final TableModel table) {
        this.table = table;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public List<OrderEntry> getOrderEntries() {
        return List.copyOf(orderEntries);
    }

    public void setOrderEntries(final List<OrderEntry> orderEntries) {
        this.orderEntries = List.copyOf(orderEntries);
    }

    public boolean isFulfilled() {
        return fulfilled;
    }

    public void setFulfilled(final boolean fulfilled) {
        this.fulfilled = fulfilled;
    }

    public LocalDateTime getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(final LocalDateTime orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }
}
