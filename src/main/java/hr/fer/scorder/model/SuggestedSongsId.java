package hr.fer.scorder.model;

import java.io.Serializable;

public class SuggestedSongsId implements Serializable {

    private Long cafe;

    private Long song;

    public SuggestedSongsId() {
    }

    public SuggestedSongsId(Long cafe_id, Long song_id) {
        this.cafe = cafe_id;
        this.song = song_id;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SuggestedSongsId)) {
            return false;
        }
        SuggestedSongsId suggestedSongsId = (SuggestedSongsId) o;

        boolean cafeEquals = (this.cafe == null && suggestedSongsId.cafe == null)
                || this.cafe != null && this.cafe.equals(suggestedSongsId.cafe);

        boolean rbrEquals = (this.song == null && suggestedSongsId.song == null)
                || this.song != null && this.song.equals(suggestedSongsId.song);

        return cafeEquals && rbrEquals;
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (cafe != null) {
            result = 31 * result + cafe.hashCode();
        }
        if (song != null) {
            result = 31 * result + song.hashCode();
        }
        return result;
    }

    public Long getCafe() {
        return cafe;
    }

    public void setCafe(Long cafe) {
        this.cafe = cafe;
    }

    public Long getSong() {
        return song;
    }

    public void setSong(Long song) {
        this.song = song;
    }

}
