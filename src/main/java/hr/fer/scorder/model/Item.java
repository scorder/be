package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "cafe_id")
    private Cafe cafe;

    @Column(nullable = false, name = "name")
    private String name;

    @Column(name = "category")
    private String category;

    @Column(nullable = false, name = "price")
    private Double price;

    @Column(nullable = false, name = "serving_size")
    private Double servingSize;

    @Column(nullable = false, name = "unit_of_measure")
    private String unitOfMeasure;

    @Column(nullable = false, name = "percent_discount")
    private Double percentDiscount;

    public Item() {
    }

    public Item(final Long id, final Cafe cafe, final String name, final String category, final Double price,
                final Double servingSize, final String unitOfMeasure, final Double percentDiscount) {
        this.id = id;
        this.cafe = cafe;
        this.name = name;
        this.category = category;
        this.price = price;
        this.servingSize = servingSize;
        this.unitOfMeasure = unitOfMeasure;
        this.percentDiscount = percentDiscount;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public Double getServingSize() {
        return servingSize;
    }

    public void setServingSize(final Double servingSize) {
        this.servingSize = servingSize;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(final String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Double getPercentDiscount() {
        return percentDiscount;
    }

    public void setPercentDiscount(final Double percentDiscount) {
        this.percentDiscount = percentDiscount;
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(final Cafe cafe) {
        this.cafe = cafe;
    }
}
