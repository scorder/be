package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cafe_current_rbr")
public class CafeCurrentRbr {

    @OneToOne
    @JoinColumn(name = "cafe_id")
    Cafe cafe;
    @Column(name = "rbr")
    Long rbr;
    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;

    public CafeCurrentRbr() {

    }

    public CafeCurrentRbr(Long id, Cafe cafe, Long rbr) {
        this.id = id;
        this.cafe = cafe;
        if (rbr == null) {
            this.rbr = (long) 1;
        } else {
            this.rbr = rbr;
        }
    }

    public Cafe getCafe() {
        return cafe;
    }

    public void setCafe(Cafe cafe) {
        this.cafe = cafe;
    }

    public Long getRbr() {
        return rbr;
    }

    public void setRbr(Long rbr) {
        this.rbr = rbr;
    }
}
