package hr.fer.scorder.model;

import hr.fer.scorder.model.generator.IdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;


@Entity
@Table(name = "song", uniqueConstraints = @UniqueConstraint(columnNames = {"song_title", "artist_name", "album_title"}))
public class Song {

    @OneToMany(mappedBy = "song")
    Set<SongList> songList;
    @Id
    @GeneratedValue(generator = IdGenerator.generatorName)
    @GenericGenerator(name = IdGenerator.generatorName, strategy = "hr.fer.scorder.model.generator.IdGenerator")
    @Column(name = "id")
    private Long id;
    @Column(nullable = false, name = "song_title")
    private String songTitle;
    @Column(nullable = false, name = "artist_name")
    private String artistName;
    @Column(nullable = true, name = "album_title")
    private String albumTitle;
    @Column(nullable = false, name = "song_link")
    private String songLink;

    public Song() {
    }

    public Song(Long id, String songTitle, String artistName, String albumTitle, String songLink) {
        this.id = id;
        this.songTitle = songTitle;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.songLink = songLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getSongLink() {
        return songLink;
    }

    public void setSongLink(String songLink) {
        this.songLink = songLink;
    }

}