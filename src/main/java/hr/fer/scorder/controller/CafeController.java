package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.CafeRequestDto;
import hr.fer.scorder.dtos.request.ElevateRequestDto;
import hr.fer.scorder.dtos.request.LoginRequestDto;
import hr.fer.scorder.dtos.request.NearbyCafesRequestDto;
import hr.fer.scorder.dtos.response.CafeExtendedResponseDto;
import hr.fer.scorder.dtos.response.CafeResponseDto;
import hr.fer.scorder.dtos.response.JwtResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.mapper.impl.CafeUpdateMapper;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.TableModel;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cafe")
public class CafeController {

    final CafeService cafeService;

    final TableService tableService;

    final Mapper<Cafe, CafeResponseDto> cafeCafeResponseDtoMapper;

    final Mapper<CafeRequestDto, Cafe> cafeRequestDtoCafeMapper;

    final CafeUpdateMapper cafeUpdateMapper;

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    @Autowired
    public CafeController(final CafeService cafeService,
                          final TableService tableService,
                          final Mapper<Cafe, CafeResponseDto> cafeCafeResponseDtoMapper,
                          final Mapper<CafeRequestDto, Cafe> cafeRequestDtoCafeMapper,
                          final CafeUpdateMapper cafeUpdateMapper,
                          final AuthenticationManager authenticationManager,
                          final JwtUtils jwtUtils) {
        this.cafeService = cafeService;
        this.tableService = tableService;
        this.cafeCafeResponseDtoMapper = cafeCafeResponseDtoMapper;
        this.cafeRequestDtoCafeMapper = cafeRequestDtoCafeMapper;
        this.cafeUpdateMapper = cafeUpdateMapper;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping("/{id}")
    public ResponseEntity<CafeResponseDto> getCafe(@PathVariable final Long id) {
        final Cafe cafe = cafeService.findCafe(id);
        return ResponseEntity.ok(cafeCafeResponseDtoMapper.map(cafe));
    }

    @GetMapping("/current")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<CafeResponseDto> getCurrentCafe() {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        final Cafe cafe = cafeService.findCafeByEmail(emailOptional.get());
        return ResponseEntity.ok(cafeCafeResponseDtoMapper.map(cafe));
    }

    @PatchMapping("/update")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<CafeResponseDto> updateCafe(@RequestBody final CafeRequestDto cafeRequestDto) {
        final Cafe originalCafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());
        final Cafe cafeToUpdate = cafeUpdateMapper.update(originalCafe, cafeRequestDto);
        final Cafe savedCafe = cafeService.addUpdateCafe(cafeToUpdate);
        return ResponseEntity.ok(cafeCafeResponseDtoMapper.map(savedCafe));
    }

    @PostMapping("/register")
    public ResponseEntity<CafeResponseDto> addCafe(@RequestBody final CafeRequestDto cafeRequestDto) {
        final Cafe cafeToAdd = cafeRequestDtoCafeMapper.map(cafeRequestDto);
        final Cafe savedCafe = cafeService.addUpdateCafe(cafeToAdd);
        return ResponseEntity.created(URI.create("/api/cafe/" + savedCafe.getId())).build();
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDto> authenticate(@RequestBody final LoginRequestDto loginRequestDto) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDto.getEmail(), loginRequestDto.getPassword(), List.of((GrantedAuthority) () -> "cafe")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);
        final Cafe cafe = cafeService.findCafeByEmail(loginRequestDto.getEmail());
        return ResponseEntity.ok(new JwtResponseDto(jwt, authentication.getPrincipal().toString(), cafe.getId(), "cafe"));
    }

    @PostMapping("/elevate")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<JwtResponseDto> elevate(@RequestBody final ElevateRequestDto elevateRequestDto) {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        final Cafe cafe = cafeService.findCafeByEmail(emailOptional.get());
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(cafe.getEmail(), elevateRequestDto.getPin(), List.of((GrantedAuthority) () -> "owner", (GrantedAuthority) () -> "cafe")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);
        return ResponseEntity.ok(new JwtResponseDto(jwt, authentication.getPrincipal().toString(), cafe.getId(), "owner"));
    }

    @GetMapping("/test")
    @PreAuthorize("hasAuthority('owner')")
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("ayyy");
    }

    @PostMapping("/nearby")
    public ResponseEntity<List<CafeExtendedResponseDto>> getNearbyCafes(@RequestBody final NearbyCafesRequestDto request) {
        Double x = request.getLatitude();
        Double y = request.getLongitude();
        List<Cafe> cafes = cafeService.findNearbyCafes(x, y, request.getLatitudeDeviation(), request.getLongitudeDeviation());
        List<CafeExtendedResponseDto> cafesResponse = new ArrayList<CafeExtendedResponseDto>();
        int taken;
        for (Cafe cafe : cafes) {
            List<TableModel> tabels = tableService.findTablesByCafe(cafe.getId());
            taken = 0;
            for (TableModel table : tabels) {
                if (table.isOccupied()) {
                    taken++;
                }
            }
            CafeExtendedResponseDto cafeExtendedResponseDto = new CafeExtendedResponseDto(cafe.getId(), cafe.getName(), cafe.getEmail(), cafe.getLocationX(), cafe.getLocationY(), taken, tabels.size());
            cafesResponse.add(cafeExtendedResponseDto);
        }
        return ResponseEntity.ok(cafesResponse);
    }
}
