package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.AcceptedSongsRequestDto;
import hr.fer.scorder.dtos.request.PlaylistRequestDto;
import hr.fer.scorder.dtos.request.SongRequestDto;
import hr.fer.scorder.dtos.request.SongsToAddToSongListRequestDto;
import hr.fer.scorder.dtos.request.SuggestedSongRequestDto;
import hr.fer.scorder.dtos.response.ExcludedSongsDto;
import hr.fer.scorder.dtos.response.SongResponseDto;
import hr.fer.scorder.dtos.response.SongResponseWrapperDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.ActivePlaylist;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.CafeCurrentRbr;
import hr.fer.scorder.model.Song;
import hr.fer.scorder.model.SongList;
import hr.fer.scorder.model.SuggestedSongs;
import hr.fer.scorder.model.TableModel;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.ActivePlaylistService;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.SongListService;
import hr.fer.scorder.service.SongService;
import hr.fer.scorder.service.SuggestedSongsService;
import hr.fer.scorder.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cafe/songs")
public class SongController {

    final SongService songService;

    final SongListService songListService;

    final CafeService cafeService;

    final TableService tableService;

    final ActivePlaylistService activePlaylistService;

    final SuggestedSongsService suggestedSongsService;

    final Mapper<Song, SongResponseDto> songSongResponseDtoMapper;

    final Mapper<SongRequestDto, Song> songRequestDtoSongMapper;

    private final JwtUtils jwtUtils;

    @Autowired
    public SongController(final SongService songService,
                          final SongListService songListService,
                          final CafeService cafeService,
                          final TableService tableService,
                          final ActivePlaylistService activePlaylistService,
                          final SuggestedSongsService suggestedSongsService,
                          final Mapper<Song, SongResponseDto> songSongResponseDtoMapper,
                          final Mapper<SongRequestDto, Song> songRequestDtoSongMapper,
                          final JwtUtils jwtUtils) {
        this.songService = songService;
        this.songListService = songListService;
        this.cafeService = cafeService;
        this.tableService = tableService;
        this.activePlaylistService = activePlaylistService;
        this.suggestedSongsService = suggestedSongsService;
        this.songSongResponseDtoMapper = songSongResponseDtoMapper;
        this.songRequestDtoSongMapper = songRequestDtoSongMapper;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping("/addSongs")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<SongResponseWrapperDto>> listSongs() {

        List<SongResponseWrapperDto> listOfSongs = new ArrayList<SongResponseWrapperDto>();

        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        final Cafe currentCafe = cafeService.findCafeByEmail(emailOptional.get());

        Boolean isInSongList;
        List<SongList> songList = songListService.findSongListByCafe(currentCafe.getId());
        List<Song> containedSongs = new ArrayList<Song>();

        for (SongList song : songList) {
            containedSongs.add(song.getSong());
        }

        for (Song song : songService.findAllSongs()) {
            if (containedSongs.contains(song)) {
                isInSongList = true;
                listOfSongs.add(new SongResponseWrapperDto(isInSongList, song.getId(), song.getSongTitle(), song.getArtistName(), song.getAlbumTitle(), song.getSongLink()));
            } else {
                isInSongList = false;
                listOfSongs.add(new SongResponseWrapperDto(isInSongList, song.getId(), song.getSongTitle(), song.getArtistName(), song.getAlbumTitle(), song.getSongLink()));
            }
        }

        return ResponseEntity.ok(listOfSongs);
    }

    @PostMapping("/addSongs")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<String> addSongs(@RequestBody final SongsToAddToSongListRequestDto songsToAddAndRemove) {

        StringBuilder sb = new StringBuilder();
        StringBuilder sbR = new StringBuilder();
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        Cafe currentCafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());
        Long songId;
        for (String stringId : songsToAddAndRemove.getSongsToAdd()) {
            sb.append(" " + stringId + ",");
            songId = Long.parseLong(stringId);
            SongList songList = new SongList(null, currentCafe, songService.findSong(songId));
            songListService.addSongList(songList);
        }
        for (String stringId : songsToAddAndRemove.getSongsToRemove()) {
            sbR.append(" " + stringId + ",");
            songId = Long.parseLong(stringId);
            SongList elementToDelete = songListService.findSongInCafesSongList(currentCafe.getId(), songId);
            songListService.deleteSongFromSongList(elementToDelete);
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        if (sbR.length() > 0) {
            sbR.setLength(sbR.length() - 1);
        }
        return ResponseEntity.ok("Added " + (sb.length() == 0 ? "no " : "") + "songs" + sb.toString() + ". Removed " + (sbR.length() == 0 ? "no " : "") + "songs" + sbR.toString() + ".");
        // string ce za 0 poslanih id-eva vratit "Added no songs.", a za 1+ (eg. [ 1, 3, 5 ,6]) "Added songs 1, 3, 5, 6."
    }

    @PostMapping("/newSong")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<SongResponseDto> newSong(@RequestBody final SongRequestDto songRequestDto) {
        final Song songToAdd = songRequestDtoSongMapper.map(songRequestDto);
        final Song songAdded = songService.addSong(songToAdd);
        return ResponseEntity.ok(songSongResponseDtoMapper.map(songAdded));
    }

    @PostMapping("/order")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<Void> addToPlaylist(@RequestBody final PlaylistRequestDto playlistRequestDto) {
        TableModel tableModel = tableService.findTable(playlistRequestDto.getTableId());
        Cafe cafe = tableModel.getCafe();

        List<SongList> songList = songListService.findSongListByCafe(cafe.getId());
        List<Long> cafesSongIds = new ArrayList<>();
        for (SongList songListElement : songList) {
            cafesSongIds.add(songListElement.getSong().getId());
        }

        for (Long songId : playlistRequestDto.getSongIds()) {
            if (!cafesSongIds.contains(songId)) {
                return new ResponseEntity("songIds contains invalid id.", HttpStatus.BAD_REQUEST);
            }
        }

        ExcludedSongsDto excludedSongsDto = getExcludedSongs(cafe.getId());
        List<Long> excludedSongs = new ArrayList<>();
        excludedSongs.addAll(excludedSongsDto.getExcludedPreviousSongIds());
        excludedSongs.addAll(excludedSongsDto.getExcludedFutureSongIds());

        for (Long songId : playlistRequestDto.getSongIds()) {
            if (!excludedSongs.contains(songId)) {
                ActivePlaylist activePlaylistEntry = new ActivePlaylist(cafe, songService.findSong(songId), (long) activePlaylistService.findActivePlaylists(cafe.getId()).size());
                activePlaylistService.addSongToPlaylist(activePlaylistEntry);
            }
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/excludedSongs")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<ExcludedSongsDto> getExcludedSongsRoute(@RequestParam final Long cafeId) {
        return ResponseEntity.ok(getExcludedSongs(cafeId));
    }

    @GetMapping("/songsQueue")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<List<SongResponseDto>> getActivePlaylist(@RequestParam final Long cafeId) {
        Long currentRbr;
        try {
            CafeCurrentRbr cafeCurrentRbr = cafeService.findRbrByCafe_Id(cafeId);
            currentRbr = cafeCurrentRbr.getRbr();
        } catch (EntityNotFoundException ex) {
            return ResponseEntity.ok(new ArrayList<>());
        }

        return ResponseEntity.ok(
                activePlaylistService
                        .findPlaylist(cafeId, currentRbr - 2)
                        .stream()
                        .map(elem -> songSongResponseDtoMapper.map(elem.getSong()))
                        .collect(Collectors.toList())
        );
    }

    @GetMapping("/suggest")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<SongResponseDto>> getSuggestedSongs() {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        Cafe currentCafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());

        List<SuggestedSongs> listOfSuggestions = suggestedSongsService.findSuggestions(currentCafe.getId());
        List<Song> songs = new ArrayList<Song>();
        for (SuggestedSongs suggestion : listOfSuggestions) {
            Song song = suggestion.getSong();
            songs.add(song);
        }
        return ResponseEntity.ok(songSongResponseDtoMapper.mapToList(songs));

    }

    @PostMapping("/suggest/accept")
    @PreAuthorize("hasAuthority('owner')")
    public ResponseEntity<String> acceptSuggestion(@RequestBody AcceptedSongsRequestDto acceptedSongsRequestDto) {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        Cafe currentCafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());
        StringBuilder sb = new StringBuilder();
        for (Long songId : acceptedSongsRequestDto.getSongIds()) {
            SuggestedSongs suggestion = suggestedSongsService.findSuggestionByCafe_IdAndSong_Id(currentCafe.getId(), songId);
            suggestedSongsService.removeSuggestion(suggestion);
            SongList newSongListElement = new SongList(null, currentCafe, songService.findSong(songId));
            songListService.addSongList(newSongListElement);
            sb.append(" " + Long.toString(songId) + ",");
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return ResponseEntity.ok("Accepted " + (sb.length() == 0 ? "no " : "") + "songs" + sb.toString() + ".");
    }


    @PostMapping("/suggest/reject")
    @PreAuthorize("hasAuthority('owner')")
    public ResponseEntity<String> rejectSuggestion(@RequestBody AcceptedSongsRequestDto acceptedSongsRequestDto) {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        Cafe currentCafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());
        StringBuilder sb = new StringBuilder();
        for (Long songId : acceptedSongsRequestDto.getSongIds()) {
            SuggestedSongs suggestion = suggestedSongsService.findSuggestionByCafe_IdAndSong_Id(currentCafe.getId(), songId);
            suggestedSongsService.removeSuggestion(suggestion);
        }
        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }
        return ResponseEntity.ok("Rejected " + (sb.length() == 0 ? "no " : "") + "songs" + sb.toString() + ".");
    }

    @PostMapping("/suggest")
    public ResponseEntity suggestSongToAddToSongList(@RequestBody SuggestedSongRequestDto suggestedSongRequestDto) {
        Song song;
        List<SongList> songList = songListService.findSongListByCafe(suggestedSongRequestDto.getCafeId());
        List<Long> songIds = new ArrayList<Long>();
        for (SongList songListElement : songList) {
            songIds.add(songListElement.getSong().getId());
        }
        try {
            Song existingSong = songService.findSongBySongTitleAndArtistNameAndAlbumTitle(suggestedSongRequestDto.getSongTitle(), suggestedSongRequestDto.getArtistName(), suggestedSongRequestDto.getAlbumTitle());
            song = existingSong;
            if (songIds.contains(song.getId())) {
                return new ResponseEntity("Cafe already has this song in it's list of songs", HttpStatus.BAD_REQUEST);
            }
        } catch (EntityNotFoundException e) {
            Song newSong = new Song(null,
                    suggestedSongRequestDto.getSongTitle(),
                    suggestedSongRequestDto.getArtistName(),
                    suggestedSongRequestDto.getAlbumTitle(),
                    suggestedSongRequestDto.getSongLink());
            song = songService.addSong(newSong);
        }
        try {
            SuggestedSongs suggestedSong = new SuggestedSongs(cafeService.findCafe(suggestedSongRequestDto.getCafeId()), song);
            suggestedSongsService.addASuggestion(suggestedSong);
        } catch (Exception e) {
            return new ResponseEntity("This song has already been suggested", HttpStatus.ALREADY_REPORTED);
        }
        return new ResponseEntity("Suggestion has been sent", HttpStatus.ACCEPTED);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<List<SongResponseDto>> getListOfSongsForCafe(@RequestParam final Long id) {
        List<SongList> listOfSongs = songListService.findSongListByCafe(id);
        List<SongResponseDto> songList = new ArrayList<>();
        for (SongList songListElement : listOfSongs) {
            SongResponseDto songResponseDto = songSongResponseDtoMapper.map(songListElement.getSong());
            songList.add(songResponseDto);
        }
        return ResponseEntity.ok(songList);
    }


    private ExcludedSongsDto getExcludedSongs(final Long cafeId) {
        Long currentRbr = null;
        try {
            CafeCurrentRbr cafeCurrentRbr = cafeService.findRbrByCafe_Id(cafeId);
            currentRbr = cafeCurrentRbr.getRbr();
        } catch (EntityNotFoundException ex) {
            new ExcludedSongsDto();
        }

        List<Long> excludedPreviousSongIds = new ArrayList<>();
        for (long i = -5; i < 0; i++) {
            try {
                ActivePlaylist activePlaylistElement = activePlaylistService.findCurrentSong(cafeId, currentRbr + i);
                excludedPreviousSongIds.add(activePlaylistElement.getSong().getId());
            } catch (EntityNotFoundException e) {
            }
        }

        List<Long> excludedFutureSongIds = new ArrayList<Long>();
        Integer playlistSize = activePlaylistService.findActivePlaylists(cafeId).size() - currentRbr.intValue();
        for (long i = 0; i < playlistSize; i++) {
            try {
                ActivePlaylist activePlaylistElement = activePlaylistService.findCurrentSong(cafeId, currentRbr + i);
                excludedFutureSongIds.add(activePlaylistElement.getSong().getId());
            } catch (EntityNotFoundException e) {
            }
        }

        return new ExcludedSongsDto(excludedPreviousSongIds, excludedFutureSongIds);
    }
}
