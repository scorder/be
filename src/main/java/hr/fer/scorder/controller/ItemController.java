package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.ItemRequestDto;
import hr.fer.scorder.dtos.response.ItemResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.Item;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cafe/item")
public class ItemController {

    private final ItemService itemService;

    private final CafeService cafeService;

    private final Mapper<Item, ItemResponseDto> itemItemResponseDtoMapper;

    private final Mapper<ItemRequestDto, Item> itemRequestDtoItemMapper;

    @Autowired
    public ItemController(final ItemService itemService,
                          final CafeService cafeService,
                          final Mapper<Item, ItemResponseDto> itemItemResponseDtoMapper,
                          final Mapper<ItemRequestDto, Item> itemRequestDtoItemMapper) {
        this.itemService = itemService;
        this.cafeService = cafeService;
        this.itemItemResponseDtoMapper = itemItemResponseDtoMapper;
        this.itemRequestDtoItemMapper = itemRequestDtoItemMapper;
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<ItemResponseDto> addItem(@RequestBody final ItemRequestDto itemRequestDto) {
        final Item savedItem = itemService.addItem(itemRequestDtoItemMapper.map(itemRequestDto));
        return ResponseEntity.ok(itemItemResponseDtoMapper.map(savedItem));
    }

    @GetMapping("/pricelist")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<ItemResponseDto>> getPriceList() {
        final String email = JwtUtils.getCurrentUserUsername().get();
        final Cafe currentCafe = cafeService.findCafeByEmail(email);
        final List<Item> items = itemService.getPriceList(currentCafe.getId());
        return ResponseEntity.ok(itemItemResponseDtoMapper.mapToList(items));
    }

    @GetMapping("/menu")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<List<ItemResponseDto>> getMenu(@RequestParam final Long cafeId) {
        final List<Item> items = itemService.getPriceList(cafeId);
        return ResponseEntity.ok(itemItemResponseDtoMapper.mapToList(items));
    }
}
