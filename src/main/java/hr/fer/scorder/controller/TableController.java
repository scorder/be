package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.OccupyRequestDto;
import hr.fer.scorder.dtos.response.ScanResponseDto;
import hr.fer.scorder.dtos.response.TableResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.Event;
import hr.fer.scorder.model.TableModel;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.EventService;
import hr.fer.scorder.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cafe/tables")
public class TableController {

    private final TableService tableService;

    private final CafeService cafeService;

    private final EventService eventService;

    private final Mapper<TableModel, TableResponseDto> tableModelTableResponseDtoMapper;

    private final Mapper<TableModel, ScanResponseDto> tableModelScanResponseDtoMapper;

    @Autowired
    public TableController(final TableService tableService, final CafeService cafeService,
                           final EventService eventService, final Mapper<TableModel, TableResponseDto> tableModelTableResponseDtoMapper,
                           final Mapper<TableModel, ScanResponseDto> tableModelScanResponseDtoMapper) {
        this.tableService = tableService;
        this.cafeService = cafeService;
        this.eventService = eventService;
        this.tableModelTableResponseDtoMapper = tableModelTableResponseDtoMapper;
        this.tableModelScanResponseDtoMapper = tableModelScanResponseDtoMapper;
    }

    @GetMapping("/get")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<TableResponseDto>> getMyTables() {
        final String currentCafeEmail = JwtUtils.getCurrentUserUsername().get();
        final Cafe currentCafe = cafeService.findCafeByEmail(currentCafeEmail);
        final List<TableModel> tables = tableService.findTablesByCafe(currentCafe.getId());

        return ResponseEntity.ok(tableModelTableResponseDtoMapper.mapToList(tables));
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<TableResponseDto>> addTables(@RequestParam Integer numberOfTables) {
        final String currentCafeEmail = JwtUtils.getCurrentUserUsername().get();
        final Cafe currentCafe = cafeService.findCafeByEmail(currentCafeEmail);
        final List<TableModel> currentTables = tableService.findTablesByCafe(currentCafe.getId());
        final List<TableModel> newTables = new ArrayList<>();
        for (int i = currentTables.size() + 1; i <= currentTables.size() + numberOfTables; i++) {
            newTables.add(new TableModel(null, currentCafe, i, false));
        }
        return ResponseEntity.ok(tableModelTableResponseDtoMapper.mapToList(tableService.addAllTables(newTables)));
    }

    @PostMapping("/occupy")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<?> tableOccupied(@RequestBody final OccupyRequestDto occupyRequestDto) {
        tableService.setOccupied(occupyRequestDto.getTableNumber(), true);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/vacant")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<?> tableVacant(@RequestBody final OccupyRequestDto occupyRequestDto) {
        tableService.setOccupied(occupyRequestDto.getTableNumber(), false);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/leave")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<?> tableVacantAndroid(@RequestParam final Long tableId) {
        tableService.setOccupied(tableId, false);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/scan")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<ScanResponseDto> handleTableScan(@RequestParam Long tableId) {
        final TableModel table = tableService.findTable(tableId);

        if (table.isOccupied()) {
            return new ResponseEntity("Table is already occupied.", HttpStatus.CONFLICT);
        }

        tableService.setOccupied(tableId, true);
        final Optional<Event> eventOptional = eventService.findEventTodayByCafe(table.getCafe().getId());
        final ScanResponseDto scanResponseDto = tableModelScanResponseDtoMapper.map(table);
        if (eventOptional.isPresent()) {
            final Event event = eventOptional.get();
            scanResponseDto.setShortEventDesc(event.getEventName() + " u " + event.getDateStart().getHour() + ":" + event.getDateStart().getMinute());
        }

        return ResponseEntity.ok(scanResponseDto);
    }
}
