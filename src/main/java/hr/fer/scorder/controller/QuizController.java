package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.AnswerRequestDto;
import hr.fer.scorder.dtos.request.AnswersRequestDto;
import hr.fer.scorder.dtos.request.EventRequestDto;
import hr.fer.scorder.dtos.request.QuestionRequestDto;
import hr.fer.scorder.dtos.request.QuizRequestDto;
import hr.fer.scorder.dtos.response.EventResponseDto;
import hr.fer.scorder.dtos.response.QuestionResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Event;
import hr.fer.scorder.model.Question;
import hr.fer.scorder.model.QuestionResponse;
import hr.fer.scorder.model.User;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.EventService;
import hr.fer.scorder.service.QuestionService;
import hr.fer.scorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("cafe/events/quiz")
public class QuizController {

    private final QuestionService questionService;

    private final EventService eventService;

    private final UserService userService;

    private final Mapper<QuestionRequestDto, Question> questionRequestDtoQuestionMapper;

    private final Mapper<Question, QuestionResponseDto> questionQuestionResponseDtoMapper;

    private final Mapper<EventRequestDto, Event> eventRequestDtoEventMapper;

    private final Mapper<QuizRequestDto, EventRequestDto> quizRequestDtoEventRequestDtoMapper;

    @Autowired
    public QuizController(QuestionService questionService,
                          EventService eventService,
                          UserService userService,
                          Mapper<QuestionRequestDto, Question> questionRequestDtoQuestionMapper,
                          Mapper<Question, QuestionResponseDto> questionQuestionResponseDtoMapper,
                          final Mapper<EventRequestDto, Event> eventRequestDtoEventMapper,
                          final Mapper<QuizRequestDto, EventRequestDto> quizRequestDtoEventRequestDtoMapper) {
        this.questionService = questionService;
        this.eventService = eventService;
        this.userService = userService;
        this.questionRequestDtoQuestionMapper = questionRequestDtoQuestionMapper;
        this.questionQuestionResponseDtoMapper = questionQuestionResponseDtoMapper;
        this.eventRequestDtoEventMapper = eventRequestDtoEventMapper;
        this.quizRequestDtoEventRequestDtoMapper = quizRequestDtoEventRequestDtoMapper;
    }

    @GetMapping("{eventId}")
    public ResponseEntity<List<QuestionResponseDto>> getListOfQuestionsForEvent(@PathVariable final Long eventId) {
        List<Question> questions = questionService.findQuestionsByEvent(eventId);
        List<QuestionResponseDto> questionList = new ArrayList<>();
        for (Question question : questions) {
            QuestionResponseDto questionResponseDto = questionQuestionResponseDtoMapper.map(question);
            questionList.add(questionResponseDto);
        }
        return ResponseEntity.ok(questionList);
    }

    @PostMapping("/answers")
    @PreAuthorize("hasAuthority('user')")
    public List<Integer> getResult(@RequestBody final AnswersRequestDto answersRequestDto) {
        final User currentUser = userService.findUserByEmail(JwtUtils.getCurrentUserUsername().get());
        int correctCounter = 0;
        final List<AnswerRequestDto> answerRequestDtoList = answersRequestDto.getAnswerRequestDtoList();
        final List<Integer> list = new ArrayList<>();
        for (AnswerRequestDto answerRequestDto : answerRequestDtoList) {
            final String question = answerRequestDto.getQuestion();
            final Question question1 = questionService.findQuestionByQuestionAndEvent(question, answersRequestDto.getEventId());
            if (!questionService.alreadyAnswered(question1.getId(), currentUser.getId())){
                final String correctAnswer = question1.getCorrectAnswer();
                final boolean correct = answerRequestDto.getAnswer().equals(correctAnswer);
                if (correct) {
                    correctCounter++;
                }
                questionService.saveResponse(new QuestionResponse(null, question1, currentUser, correct));
            }
        }
        list.add(correctCounter);
        list.add(answerRequestDtoList.size());
        return list;
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<?> addQuiz(@RequestBody final QuizRequestDto quizRequestDto) {
        final Event quizEvent = eventRequestDtoEventMapper.map(quizRequestDtoEventRequestDtoMapper.map(quizRequestDto));
        final Event event = eventService.addEvent(quizEvent);
        final List<Question> questions = questionRequestDtoQuestionMapper.mapToList(quizRequestDto.getQuestions());
        for (Question q:questions) {
            q.setEvent(event);
        }
        questionService.addQuestions(questions);
        return ResponseEntity.ok().build();
    }
}
