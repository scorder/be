package hr.fer.scorder.controller;


import hr.fer.scorder.dtos.request.EventRequestDto;
import hr.fer.scorder.dtos.response.EventResponseDto;
import hr.fer.scorder.dtos.response.PlacementResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.Event;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.EventService;
import hr.fer.scorder.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("cafe/events")
public class EventController {

    private final EventService eventService;

    private final CafeService cafeService;

    private final QuestionService questionService;

    private final Mapper<EventRequestDto, Event> eventRequestDtoEventMapper;

    private final Mapper<Event, EventResponseDto> eventEventResponseDtoMapper;

    @Autowired
    public EventController(EventService eventService,
                           CafeService cafeService,
                           QuestionService questionService, Mapper<EventRequestDto, Event> eventRequestDtoEventMapper,
                           Mapper<Event, EventResponseDto> eventEventResponseDtoMapper) {
        this.eventService = eventService;
        this.cafeService = cafeService;
        this.questionService = questionService;
        this.eventRequestDtoEventMapper = eventRequestDtoEventMapper;
        this.eventEventResponseDtoMapper = eventEventResponseDtoMapper;
    }

    @GetMapping("/my")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<EventResponseDto>> getListOfMyEvents() {
        final Cafe cafe = cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get());
        final List<Event> listOfEvents = eventService.findEventsByCaffe(cafe.getId());
        return ResponseEntity.ok(eventEventResponseDtoMapper.mapToList(listOfEvents));
    }

    @GetMapping("{id}")
    public ResponseEntity<List<EventResponseDto>> getListOfEventsForCafe(@PathVariable final Long id) {
        List<Event> listOfEvents = eventService.findEventsByCaffe(id);
        List<EventResponseDto> eventList = new ArrayList<EventResponseDto>();
        for (Event eventListElement : listOfEvents) {
            EventResponseDto eventResponseDto = eventEventResponseDtoMapper.map(eventListElement);
            eventList.add(eventResponseDto);
        }
        return ResponseEntity.ok(eventList);
    }

    @GetMapping("/next{id}")
    public ResponseEntity<EventResponseDto> getNextEvent(@PathVariable final Long id) {
        List<Event> listOfEvents = eventService.findEventsByCaffe(id);
        LocalDateTime current = LocalDateTime.now();
        LocalDateTime next_24 = LocalDateTime.now().plusDays(1);
        for (Event event : listOfEvents) {
            if (eventService.isQuiz(event) && event.getDateStart().isBefore(next_24) & event.getDateStart().isAfter(current)) {
                return ResponseEntity.ok(eventEventResponseDtoMapper.map(event));
            }
        }
        return null;
    }

    @GetMapping("placements/{eventId}")
    public ResponseEntity<?> getPlacementsForCafe(@PathVariable final Long eventId) {
        List<Map.Entry<String, Double>> rankings = questionService.getPercentagesCorrectByUsername(eventId);
        List<PlacementResponseDto> placementResponseDtos = new LinkedList<>();
        for (int i = 0; i < rankings.size(); i++) {
            placementResponseDtos.add(new PlacementResponseDto(i + 1, rankings.get(i).getKey(), rankings.get(i).getValue().longValue()));
        }
        return ResponseEntity.ok(placementResponseDtos);

    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<EventResponseDto> addEvent(@RequestBody final EventRequestDto eventRequestDto) {

        final Event newEvent = eventService.addEvent(eventRequestDtoEventMapper.map(eventRequestDto));
        return ResponseEntity.ok(eventEventResponseDtoMapper.map(newEvent));
    }


}
