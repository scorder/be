package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.SongRequestDto;
import hr.fer.scorder.dtos.response.SongResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.ActivePlaylist;
import hr.fer.scorder.model.Cafe;
import hr.fer.scorder.model.CafeCurrentRbr;
import hr.fer.scorder.model.Song;
import hr.fer.scorder.model.SongList;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.ActivePlaylistService;
import hr.fer.scorder.service.CafeService;
import hr.fer.scorder.service.SongListService;
import hr.fer.scorder.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cafe/playlist")
public class PlaylistController {

    final SongService songService;

    final ActivePlaylistService activePlaylistService;

    final CafeService cafeService;

    final SongListService songListService;

    final Mapper<Song, SongResponseDto> songSongResponseDtoMapper;

    final Mapper<SongRequestDto, Song> songRequestDtoSongMapper;

    private final JwtUtils jwtUtils;

    @Autowired
    public PlaylistController(final SongService songService,
                              final ActivePlaylistService activePlaylistService,
                              final CafeService cafeService,
                              final SongListService songListService,
                              final Mapper<Song, SongResponseDto> songSongResponseDtoMapper,
                              final Mapper<SongRequestDto, Song> songRequestDtoSongMapper,
                              final JwtUtils jwtUtils) {
        this.songService = songService;
        this.activePlaylistService = activePlaylistService;
        this.cafeService = cafeService;
        this.songListService = songListService;
        this.songSongResponseDtoMapper = songSongResponseDtoMapper;
        this.songRequestDtoSongMapper = songRequestDtoSongMapper;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping("/nextSongs")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<SongResponseDto>> nextSongs() {

        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        final Cafe currentCafe = cafeService.findCafeByEmail(emailOptional.get());

        CafeCurrentRbr cafeCurrentRbr = cafeService.findRbrByCafe_Id(currentCafe.getId());
        Long rbr = cafeCurrentRbr.getRbr();

        List<SongResponseDto> playlist = new ArrayList<SongResponseDto>();
        List<ActivePlaylist> activePlaylist = activePlaylistService.findPlaylist(currentCafe.getId(), rbr);
        for (ActivePlaylist playlistElement : activePlaylist) {
            Song song = playlistElement.getSong();
            SongResponseDto songResponseDto = songSongResponseDtoMapper.map(song);
            playlist.add(songResponseDto);
        }

        return ResponseEntity.ok(playlist);
    }

    @GetMapping("/currentSong")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<SongResponseDto> getNextSongInPlaylist() {

        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        final Cafe currentCafe = cafeService.findCafeByEmail(emailOptional.get());

        CafeCurrentRbr cafeCurrentRbr = cafeService.findRbrByCafe_Id(currentCafe.getId());
        Long rbr = cafeCurrentRbr.getRbr();
        try {
            ActivePlaylist nextSongInPlaylist = activePlaylistService.findCurrentSong(currentCafe.getId(), rbr);
            Song nextSong = nextSongInPlaylist.getSong();
            cafeService.updateCafeCurrentRbr(currentCafe, rbr + 1);
            return ResponseEntity.ok(songSongResponseDtoMapper.map(nextSong));

        } catch (EntityNotFoundException e) {
            List<Song> excludedSongIds = new ArrayList<Song>();
            List<SongList> cafeSongList = songListService.findSongListByCafe(currentCafe.getId());
            if (cafeSongList.isEmpty()) {
                throw new EntityNotFoundException("Cafe doesn't have any songs in it's list of songs.");
            }
            if (cafeSongList.size() > 20) {
                for (long i = -10; i < 5; i++) {
                    try {
                        ActivePlaylist activePlaylistElement = activePlaylistService.findCurrentSong(currentCafe.getId(), rbr + i);
                        excludedSongIds.add(activePlaylistElement.getSong());
                    } catch (EntityNotFoundException f) {
                    }
                }
            }

            Song songToPlay = null;
            do {
                List<SongList> songList = songListService.findSongListByCafe(currentCafe.getId());
                SongList selectedElement = songList.get((int) Math.random() % songList.size());
                songToPlay = selectedElement.getSong();
            } while (excludedSongIds.contains(songToPlay) && songToPlay != null);
            return ResponseEntity.ok(songSongResponseDtoMapper.map(songToPlay));
        }
    }
}    
