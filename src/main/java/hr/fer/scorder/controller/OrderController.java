package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.OrderFulfillDto;
import hr.fer.scorder.dtos.request.OrderRequestDto;
import hr.fer.scorder.dtos.response.OrderResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.mapper.impl.OrderEntryRequestDtoOrderEntryMapper;
import hr.fer.scorder.model.Order;
import hr.fer.scorder.model.OrderEntry;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.OrderEntryService;
import hr.fer.scorder.service.OrderService;
import hr.fer.scorder.service.TableService;
import hr.fer.scorder.service.impl.ItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    private final SimpMessagingTemplate simpMessagingTemplate;

    private final OrderService orderService;

    private final Mapper<OrderRequestDto, Order> orderRequestDtoOrderMapper;

    private final Mapper<Order, OrderResponseDto> orderOrderResponseDtoMapper;

    private final OrderEntryService orderEntryService;

    private final OrderEntryRequestDtoOrderEntryMapper orderEntryRequestDtoOrderEntryMapper;

    private final ItemServiceImpl itemService;

    private final TableService tableService;

    @Autowired
    public OrderController(final SimpMessagingTemplate simpMessagingTemplate,
                           final OrderService orderService,
                           final Mapper<OrderRequestDto, Order> orderRequestDtoOrderMapper,
                           final Mapper<Order, OrderResponseDto> orderOrderResponseDtoMapper,
                           final OrderEntryService orderEntryService,
                           final OrderEntryRequestDtoOrderEntryMapper orderEntryRequestDtoOrderEntryMapper,
                           final ItemServiceImpl itemService,
                           final TableService tableService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.orderService = orderService;
        this.orderRequestDtoOrderMapper = orderRequestDtoOrderMapper;
        this.orderOrderResponseDtoMapper = orderOrderResponseDtoMapper;
        this.orderEntryService = orderEntryService;
        this.orderEntryRequestDtoOrderEntryMapper = orderEntryRequestDtoOrderEntryMapper;
        this.itemService = itemService;
        this.tableService = tableService;
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<Void> addOrder(@RequestBody final OrderRequestDto orderRequestDto) {
        final Order orderToPlace = orderRequestDtoOrderMapper.map(orderRequestDto);
        final Order savedOrder = orderService.addOrder(orderToPlace);
        tableService.setOccupied(savedOrder.getTable().getId(), true);

        List<OrderEntry> entries = new ArrayList<>();
        for (var entry : orderRequestDto.getOrderEntries()) {
            entries.add(new OrderEntry(
                    null,
                    itemService.findItem(entry.getItemId()),
                    savedOrder,
                    entry.getQuantity()
            ));
        }
        orderEntryService.addOrderEntries(entries);

        simpMessagingTemplate.convertAndSend("/api/topic/neworders/" + savedOrder.getTable().getCafe().getId(),
                orderOrderResponseDtoMapper.map(savedOrder));
        return ResponseEntity.ok().build();
    }

    @PostMapping("/fulfill")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<OrderResponseDto> fulfillOrder(@RequestBody final OrderFulfillDto orderFulfillDto) {
        final Order orderToFulfill = orderService.findOrder(orderFulfillDto.getOrderId());
        //security check: order is for current cafe
        if (!orderToFulfill.getTable().getCafe().getEmail().equals(JwtUtils.getCurrentUserUsername().get())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        orderToFulfill.setFulfilled(true);
        final Order savedOrder = orderService.addOrder(orderToFulfill);
        return ResponseEntity.ok(orderOrderResponseDtoMapper.map(savedOrder));
    }

    @GetMapping("/unfulfilled")
    @PreAuthorize("hasAuthority('cafe')")
    public ResponseEntity<List<OrderResponseDto>> getUnfulfilledOrders() {
        final List<Order> orders = orderService.findUnfulfilledOrders();
        return ResponseEntity.ok(orderOrderResponseDtoMapper.mapToList(orders));
    }
}
