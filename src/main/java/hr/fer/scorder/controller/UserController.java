package hr.fer.scorder.controller;

import hr.fer.scorder.dtos.request.LoginRequestDto;
import hr.fer.scorder.dtos.request.UserRequestDto;
import hr.fer.scorder.dtos.response.JwtResponseDto;
import hr.fer.scorder.dtos.response.UserResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.mapper.impl.UserUpdateMapper;
import hr.fer.scorder.model.User;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    final UserService userService;

    final Mapper<User, UserResponseDto> userUserResponseDtoMapper;

    final Mapper<UserRequestDto, User> userRequestDtoUserMapper;

    final UserUpdateMapper userUpdateMapper;

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    @Autowired
    public UserController(final UserService userService,
                          final Mapper<User, UserResponseDto> userUserResponseDtoMapper,
                          final Mapper<UserRequestDto, User> userRequestDtoUserMapper,
                          final UserUpdateMapper userUpdateMapper,
                          final AuthenticationManager authenticationManager,
                          final JwtUtils jwtUtils) {
        this.userService = userService;
        this.userUserResponseDtoMapper = userUserResponseDtoMapper;
        this.userRequestDtoUserMapper = userRequestDtoUserMapper;
        this.userUpdateMapper = userUpdateMapper;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    @GetMapping("/current")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<UserResponseDto> getCurrentUser() {
        final Optional<String> emailOptional = JwtUtils.getCurrentUserUsername();
        if (emailOptional.isEmpty() || emailOptional.get().equals("anonymousUser")) {
            throw new UsernameNotFoundException("Not logged in.");
        }
        final User user = userService.findUserByEmail(emailOptional.get());
        return ResponseEntity.ok(userUserResponseDtoMapper.map(user));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDto> getUser(@PathVariable final Long id) {
        final User user = userService.findUser(id);
        return ResponseEntity.ok(userUserResponseDtoMapper.map(user));
    }

    @PatchMapping("/update")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<UserResponseDto> updateUser(@RequestBody final UserRequestDto userRequestDto) {
        final User originalUser = userService.findUserByEmail(JwtUtils.getCurrentUserUsername().get());
        final User updatedUser = userUpdateMapper.update(originalUser, userRequestDto);
        final User savedUser = userService.addUpdateUser(updatedUser);
        return ResponseEntity.ok(userUserResponseDtoMapper.map(savedUser));
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseDto> addUser(@RequestBody final UserRequestDto userRequestDto) {
        final User userToAdd = userRequestDtoUserMapper.map(userRequestDto);
        final User userAdded = userService.addUpdateUser(userToAdd);
        return ResponseEntity.created(URI.create("/api/user/" + userAdded.getId())).build();
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponseDto> authenticate(@RequestBody final LoginRequestDto loginRequestDto) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDto.getEmail(), loginRequestDto.getPassword(), List.of((GrantedAuthority) () -> "user")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);
        final User user = userService.findUserByEmail(loginRequestDto.getEmail());
        return ResponseEntity.ok(new JwtResponseDto(jwt, authentication.getPrincipal().toString(), user.getId(), "user"));
    }
}
