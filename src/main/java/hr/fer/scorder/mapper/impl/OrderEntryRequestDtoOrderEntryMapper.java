package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.OrderEntryRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.OrderEntry;
import hr.fer.scorder.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderEntryRequestDtoOrderEntryMapper implements Mapper<OrderEntryRequestDto, OrderEntry> {

    private final ItemService itemService;

    @Autowired
    public OrderEntryRequestDtoOrderEntryMapper(final ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public OrderEntry map(final OrderEntryRequestDto from) {
        return new OrderEntry(
                null,
                itemService.findItem(from.getItemId()),
                null,
                from.getQuantity()
        );
    }
}
