package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.OrderEntryRequestDto;
import hr.fer.scorder.dtos.request.OrderRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Order;
import hr.fer.scorder.model.OrderEntry;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.TableService;
import hr.fer.scorder.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class OrderRequestDtoOrderMapper implements Mapper<OrderRequestDto, Order> {

    private final Mapper<OrderEntryRequestDto, OrderEntry> orderEntryRequestDtoOrderEntryMapper;

    private final TableService tableService;

    private final UserService userService;

    @Autowired
    public OrderRequestDtoOrderMapper(final Mapper<OrderEntryRequestDto, OrderEntry> orderEntryRequestDtoOrderEntryMapper,
                                      final TableService tableService, final UserService userService) {
        this.orderEntryRequestDtoOrderEntryMapper = orderEntryRequestDtoOrderEntryMapper;
        this.tableService = tableService;
        this.userService = userService;
    }

    @Override
    public Order map(final OrderRequestDto from) {
        return new Order(
                null,
                tableService.findTable(from.getTableId()),
                userService.findUserByEmail(JwtUtils.getCurrentUserUsername().get()),
                orderEntryRequestDtoOrderEntryMapper.mapToList(from.getOrderEntries()),
                false,
                LocalDateTime.now()
        );
    }
}
