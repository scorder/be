package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.UserRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class UserRequestDtoUserMapper implements Mapper<UserRequestDto, User> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserRequestDtoUserMapper(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User map(final UserRequestDto from) {
        return new User(
                null,
                from.getEmail(),
                from.getUsername(),
                passwordEncoder.encode(from.getPassword()),
                from.getFirstName(),
                from.getLastName(),
                LocalDate.now());
    }
}
