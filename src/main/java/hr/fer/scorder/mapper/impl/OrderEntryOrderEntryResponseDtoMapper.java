package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.ItemResponseDto;
import hr.fer.scorder.dtos.response.OrderEntryResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Item;
import hr.fer.scorder.model.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderEntryOrderEntryResponseDtoMapper implements Mapper<OrderEntry, OrderEntryResponseDto> {

    private final Mapper<Item, ItemResponseDto> itemItemResponseDtoMapper;

    @Autowired
    public OrderEntryOrderEntryResponseDtoMapper(final Mapper<Item, ItemResponseDto> itemItemResponseDtoMapper) {
        this.itemItemResponseDtoMapper = itemItemResponseDtoMapper;
    }

    @Override
    public OrderEntryResponseDto map(final OrderEntry from) {
        return new OrderEntryResponseDto(itemItemResponseDtoMapper.map(from.getItem()), from.getQuantity());
    }
}
