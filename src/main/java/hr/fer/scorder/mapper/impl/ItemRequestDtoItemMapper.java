package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.ItemRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Item;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ItemRequestDtoItemMapper implements Mapper<ItemRequestDto, Item> {

    private final CafeService cafeService;

    @Autowired
    public ItemRequestDtoItemMapper(final CafeService cafeService) {
        this.cafeService = cafeService;
    }

    @Override
    public Item map(final ItemRequestDto from) {
        return new Item(null,
                cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get()),
                from.getName(),
                from.getCategory(),
                from.getPrice(),
                from.getServingSize(),
                from.getUnitOfMeasure(),
                from.getPercentDiscount());
    }
}
