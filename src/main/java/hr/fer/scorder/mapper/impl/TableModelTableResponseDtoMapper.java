package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.TableResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.TableModel;
import org.springframework.stereotype.Component;

@Component
public class TableModelTableResponseDtoMapper implements Mapper<TableModel, TableResponseDto> {

    @Override
    public TableResponseDto map(final TableModel from) {
        return new TableResponseDto(from.getId(), from.getTableNumber());
    }
}
