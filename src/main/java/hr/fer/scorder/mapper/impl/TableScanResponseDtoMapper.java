package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.ScanResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.TableModel;
import org.springframework.stereotype.Component;

@Component
public class TableScanResponseDtoMapper implements Mapper<TableModel, ScanResponseDto> {
    @Override
    public ScanResponseDto map(final TableModel from) {
        return new ScanResponseDto(from.getId(),
                from.getTableNumber(),
                from.getCafe().getId(),
                from.getCafe().getName(),
                from.getCafe().getLocationX(),
                from.getCafe().getLocationY(),
                null
        );
    }
}
