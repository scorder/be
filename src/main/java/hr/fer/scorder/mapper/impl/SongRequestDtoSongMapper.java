package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.SongRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Song;
import org.springframework.stereotype.Component;

@Component
public class SongRequestDtoSongMapper implements Mapper<SongRequestDto, Song> {

    @Override
    public Song map(SongRequestDto from) {
        if (from.getAlbumTitle().isEmpty()) {
            from.setAlbumTitle("Single");
        }
        return new Song(
                null,
                from.getSongTitle(),
                from.getArtistName(),
                from.getAlbumTitle(),
                from.getSongLink()
        );
    }
}
