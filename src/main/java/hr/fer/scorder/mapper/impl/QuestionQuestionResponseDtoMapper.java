package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.QuestionResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Question;
import org.springframework.stereotype.Component;

@Component
public class QuestionQuestionResponseDtoMapper implements Mapper<Question, QuestionResponseDto> {


    @Override
    public QuestionResponseDto map(Question from) {
        return new QuestionResponseDto(
                from.getQuestion(),
                from.getAnswerA(),
                from.getAnswerB(),
                from.getAnswerC(),
                from.getAnswerD()
        );
    }
}
