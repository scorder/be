package hr.fer.scorder.mapper.impl;


import hr.fer.scorder.dtos.response.EventResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Event;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class EventEventResponseDtoMapper implements Mapper<Event, EventResponseDto> {

    @Override
    public EventResponseDto map(final Event from) {
        return new EventResponseDto(
                from.getId(),
                from.getEventName(),
                Timestamp.valueOf(from.getDateStart()),
                Timestamp.valueOf(from.getEnding()),
                from.getEventInfo());
    }
}
