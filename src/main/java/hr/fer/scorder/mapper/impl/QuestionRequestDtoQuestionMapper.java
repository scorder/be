package hr.fer.scorder.mapper.impl;


import hr.fer.scorder.dtos.request.QuestionRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Question;
import hr.fer.scorder.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionRequestDtoQuestionMapper implements Mapper<QuestionRequestDto, Question> {

    @Override
    public Question map(QuestionRequestDto from) {
        return new Question(
                null,
                null,
                from.getText(),
                from.getA(),
                from.getB(),
                from.getC(),
                from.getD(),
                from.getCorrect(),
                from.getNumberOfQuestion());
    }
}
