package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.CafeResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Cafe;
import org.springframework.stereotype.Component;

@Component
public class CafeCafeResponseDtoMapper implements Mapper<Cafe, CafeResponseDto> {

    @Override
    public CafeResponseDto map(final Cafe from) {
        return new CafeResponseDto(
                from.getId(),
                from.getName(),
                from.getEmail(),
                from.getLocationX(),
                from.getLocationY()
        );
    }
}
