package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.EventRequestDto;
import hr.fer.scorder.dtos.request.QuizRequestDto;
import hr.fer.scorder.mapper.Mapper;
import org.springframework.stereotype.Component;

@Component
public class QuizRequestDtoEventRequestDtoMapper implements Mapper<QuizRequestDto, EventRequestDto> {

    @Override
    public EventRequestDto map(final QuizRequestDto from) {
        return new EventRequestDto(from.getEventName(), from.getDate(), from.getEnding(), from.getEventInfo());
    }
}
