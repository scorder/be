package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.CafeRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Cafe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class CafeRequestDtoCafeMapper implements Mapper<CafeRequestDto, Cafe> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public CafeRequestDtoCafeMapper(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Cafe map(final CafeRequestDto from) {
        return new Cafe(
                null,
                from.getEmail(),
                passwordEncoder.encode(from.getPassword()),
                passwordEncoder.encode(from.getPin()),
                from.getName(),
                from.getLocationX(),
                from.getLocationY(),
                LocalDate.now());
    }
}
