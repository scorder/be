package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.SongResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Song;
import org.springframework.stereotype.Component;

@Component
public class SongSongResponseDtoMapper implements Mapper<Song, SongResponseDto> {

    @Override
    public SongResponseDto map(final Song from) {
        return new SongResponseDto(
                from.getId(),
                from.getSongTitle(),
                from.getArtistName(),
                from.getAlbumTitle(),
                from.getSongLink()
        );
    }

}
