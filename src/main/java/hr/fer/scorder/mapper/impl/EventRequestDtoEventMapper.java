package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.EventRequestDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Event;
import hr.fer.scorder.security.jwt.JwtUtils;
import hr.fer.scorder.service.CafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class EventRequestDtoEventMapper implements Mapper<EventRequestDto, Event> {

    private final CafeService cafeService;

    @Autowired
    public EventRequestDtoEventMapper(final CafeService cafeService) {
        this.cafeService = cafeService;
    }

    @Override
    public Event map(final EventRequestDto from) {
        return new Event(
                null,
                cafeService.findCafeByEmail(JwtUtils.getCurrentUserUsername().get()),
                from.getEventName(),
                from.getDate(),
                from.getEnding(),
                from.getEventInfo()
        );
    }
}
