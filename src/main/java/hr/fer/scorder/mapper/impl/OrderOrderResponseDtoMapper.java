package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.OrderEntryResponseDto;
import hr.fer.scorder.dtos.response.OrderResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Order;
import hr.fer.scorder.model.OrderEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderOrderResponseDtoMapper implements Mapper<Order, OrderResponseDto> {

    private final Mapper<OrderEntry, OrderEntryResponseDto> orderEntryOrderEntryResponseDtoMapper;

    @Autowired
    public OrderOrderResponseDtoMapper(final Mapper<OrderEntry, OrderEntryResponseDto> orderEntryOrderEntryResponseDtoMapper) {
        this.orderEntryOrderEntryResponseDtoMapper = orderEntryOrderEntryResponseDtoMapper;
    }

    @Override
    public OrderResponseDto map(final Order from) {
        return new OrderResponseDto(from.getId(),
                from.getTable().getId(),
                orderEntryOrderEntryResponseDtoMapper.mapToList(from.getOrderEntries()),
                from.getOrderTimestamp());
    }
}
