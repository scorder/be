package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.UserRequestDto;
import hr.fer.scorder.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateMapper {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserUpdateMapper(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User update(final User toUpdate, final UserRequestDto updateFrom) {
        if (updateFrom.getEmail() != null) {
            toUpdate.setEmail(updateFrom.getEmail());
        }
        if (updateFrom.getUsername() != null) {
            toUpdate.setUsername(updateFrom.getUsername());
        }
        if (updateFrom.getPassword() != null) {
            toUpdate.setPassword(passwordEncoder.encode(updateFrom.getPassword()));
        }
        if (updateFrom.getFirstName() != null) {
            toUpdate.setFirstName(updateFrom.getFirstName());
        }
        if (updateFrom.getLastName() != null) {
            toUpdate.setLastName(updateFrom.getLastName());
        }
        return toUpdate;
    }
}
