package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.UserResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserUserResponseDtoMapper implements Mapper<User, UserResponseDto> {

    @Override
    public UserResponseDto map(final User from) {
        return new UserResponseDto(
                from.getId(),
                from.getFirstName(),
                from.getLastName(),
                from.getEmail(),
                from.getUsername());
    }
}
