package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.response.ItemResponseDto;
import hr.fer.scorder.mapper.Mapper;
import hr.fer.scorder.model.Item;
import org.springframework.stereotype.Component;

@Component
public class ItemItemResponseDtoMapper implements Mapper<Item, ItemResponseDto> {

    @Override
    public ItemResponseDto map(final Item from) {
        return new ItemResponseDto(
                from.getId(),
                from.getName(),
                from.getCategory(),
                from.getPrice(),
                from.getServingSize(),
                from.getUnitOfMeasure(),
                from.getPercentDiscount());
    }
}
