package hr.fer.scorder.mapper.impl;

import hr.fer.scorder.dtos.request.CafeRequestDto;
import hr.fer.scorder.model.Cafe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CafeUpdateMapper {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public CafeUpdateMapper(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Cafe update(final Cafe toUpdate, final CafeRequestDto updateFrom) {
        if (updateFrom.getEmail() != null) {
            toUpdate.setEmail(updateFrom.getEmail());
        }
        if (updateFrom.getPassword() != null) {
            toUpdate.setPassword(passwordEncoder.encode(updateFrom.getPassword()));
        }
        if (updateFrom.getPin() != null) {
            toUpdate.setPin(passwordEncoder.encode(updateFrom.getPin()));
        }
        if (updateFrom.getName() != null) {
            toUpdate.setName(updateFrom.getName());
        }
        if (updateFrom.getLocationX() != null) {
            toUpdate.setLocationX(updateFrom.getLocationX());
        }
        if (updateFrom.getLocationY() != null) {
            toUpdate.setLocationY(updateFrom.getLocationY());
        }
        return toUpdate;
    }
}
