package hr.fer.scorder.dtos.response;

public class OrderEntryResponseDto {

    private ItemResponseDto itemResponseDto;

    private Double quantity;

    public OrderEntryResponseDto() {
    }

    public OrderEntryResponseDto(final ItemResponseDto itemResponseDto, final Double quantity) {
        this.itemResponseDto = itemResponseDto;
        this.quantity = quantity;
    }

    public ItemResponseDto getItemResponseDto() {
        return itemResponseDto;
    }

    public void setItemResponseDto(final ItemResponseDto itemResponseDto) {
        this.itemResponseDto = itemResponseDto;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(final Double quantity) {
        this.quantity = quantity;
    }
}
