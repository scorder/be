package hr.fer.scorder.dtos.response;

public class ScanResponseDto {

    private Long tableId;

    private Integer tableNumber;

    private Long cafeId;

    private String cafeName;

    private Double locationX;

    private Double locationY;

    private String shortEventDesc;

    public ScanResponseDto() {
    }

    public ScanResponseDto(final Long tableId, final Integer tableNumber, final Long cafeId, final String cafeName,
                           final Double locationX, final Double locationY, final String shortEventDesc) {
        this.tableId = tableId;
        this.tableNumber = tableNumber;
        this.cafeId = cafeId;
        this.cafeName = cafeName;
        this.locationX = locationX;
        this.locationY = locationY;
        this.shortEventDesc = shortEventDesc;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(final Long tableId) {
        this.tableId = tableId;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(final Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Long getCafeId() {
        return cafeId;
    }

    public void setCafeId(final Long cafeId) {
        this.cafeId = cafeId;
    }

    public String getCafeName() {
        return cafeName;
    }

    public void setCafeName(final String cafeName) {
        this.cafeName = cafeName;
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(final Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(final Double locationY) {
        this.locationY = locationY;
    }

    public String getShortEventDesc() {
        return shortEventDesc;
    }

    public void setShortEventDesc(final String shortEventDesc) {
        this.shortEventDesc = shortEventDesc;
    }
}
