package hr.fer.scorder.dtos.response;

import java.time.LocalDateTime;
import java.util.List;

public class OrderResponseDto {

    private Long id;

    private Long tableId;

    private List<OrderEntryResponseDto> orderEntries;

    private LocalDateTime timestamp;

    public OrderResponseDto() {
    }

    public OrderResponseDto(final Long id, final Long tableId, final List<OrderEntryResponseDto> orderEntries,
                            final LocalDateTime timestamp) {
        this.id = id;
        this.tableId = tableId;
        this.orderEntries = orderEntries;
        this.timestamp = timestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(final Long tableId) {
        this.tableId = tableId;
    }

    public List<OrderEntryResponseDto> getOrderEntries() {
        return orderEntries;
    }

    public void setOrderEntries(final List<OrderEntryResponseDto> orderEntries) {
        this.orderEntries = orderEntries;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
