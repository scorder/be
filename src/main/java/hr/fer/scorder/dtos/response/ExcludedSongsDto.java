package hr.fer.scorder.dtos.response;

import java.util.ArrayList;
import java.util.List;

public class ExcludedSongsDto {

    private List<Long> excludedPreviousSongIds;

    private List<Long> excludedFutureSongIds;

    public ExcludedSongsDto() {
        excludedPreviousSongIds = new ArrayList<>();
        excludedFutureSongIds = new ArrayList<>();
    }

    public ExcludedSongsDto(List<Long> excludedPreviousSongIds, List<Long> excludedFutureSongIds) {
        this.excludedPreviousSongIds = excludedPreviousSongIds;
        this.excludedFutureSongIds = excludedFutureSongIds;
    }

    public List<Long> getExcludedPreviousSongIds() {
        return excludedPreviousSongIds;
    }

    public void setExcludedPreviousSongIds(List<Long> excludedPreviousSongIds) {
        this.excludedPreviousSongIds = excludedPreviousSongIds;
    }

    public List<Long> getExcludedFutureSongIds() {
        return excludedFutureSongIds;
    }

    public void setExcludedFutureSongIds(List<Long> excludedFutureSongIds) {
        this.excludedFutureSongIds = excludedFutureSongIds;
    }
}
