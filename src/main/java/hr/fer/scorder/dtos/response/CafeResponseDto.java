package hr.fer.scorder.dtos.response;

public class CafeResponseDto {

    private Long id;

    private String name;

    private String email;

    private String street;

    private String city;

    private Integer streetNumber;

    private Long zipCode;

    private Double locationX;

    private Double locationY;

    public CafeResponseDto() {

    }

    public CafeResponseDto(final Long id, final String name, final String email, final Double locationX, final Double locationY) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.locationX = locationX;
        this.locationY = locationY;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(Double locationY) {
        this.locationY = locationY;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(final Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Long getZipCode() {
        return zipCode;
    }

    public void setZipCode(final Long zipCode) {
        this.zipCode = zipCode;
    }
}