package hr.fer.scorder.dtos.response;

public class PlacementResponseDto {
    Integer place;
    String username;
    Long percentage;

    public PlacementResponseDto() {
    }

    public PlacementResponseDto(final Integer place, final String username, final Long percentage) {
        this.place = place;
        this.username = username;
        this.percentage = percentage;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(final Integer place) {
        this.place = place;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public Long getPercentage() {
        return percentage;
    }

    public void setPercentage(final Long percentage) {
        this.percentage = percentage;
    }
}
