package hr.fer.scorder.dtos.response;

public class SongResponseWrapperDto {

    public Long id;
    public String songTitle;
    public String artistName;
    public String albumTitle;
    public String songLink;
    private Boolean isInSongList;

    public SongResponseWrapperDto() {

    }

    public SongResponseWrapperDto(final Boolean isInSongList, final Long id,
                                  final String songTitle, final String artistName,
                                  final String albumTitle, final String songLink) {
        this.isInSongList = isInSongList;
        this.id = id;
        this.songTitle = songTitle;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.songLink = songLink;
    }

    public Boolean getIsInSongList() {
        return isInSongList;
    }

    public void setIsInSongList(Boolean isInSongList) {
        this.isInSongList = isInSongList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSongTitle() {
        return this.songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getArtistName() {
        return this.artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return this.albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getSongLink() {
        return this.songLink;
    }

    public void setSongLink(String songLink) {
        this.songLink = songLink;
    }
}
