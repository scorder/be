package hr.fer.scorder.dtos.response;

public class TableResponseDto {

    private Long id;

    private Integer tableNumber;

    public TableResponseDto() {
    }

    public TableResponseDto(final Long id, final Integer tableNumber) {
        this.id = id;
        this.tableNumber = tableNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(final Integer tableNumber) {
        this.tableNumber = tableNumber;
    }
}
