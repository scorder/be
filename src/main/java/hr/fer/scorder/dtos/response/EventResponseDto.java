package hr.fer.scorder.dtos.response;

import java.sql.Timestamp;

public class EventResponseDto {

    private Long id;

    private String eventName;

    private Timestamp date;

    private Timestamp ending;

    private String eventInfo;

    public EventResponseDto() {
    }

    public EventResponseDto(Long id, String eventName, Timestamp date, Timestamp ending, String eventInfo) {
        this.id = id;
        this.eventName = eventName;
        this.date = date;
        this.ending = ending;
        this.eventInfo = eventInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Timestamp getEnding() {
        return ending;
    }

    public void setEnding(Timestamp ending) {
        this.ending = ending;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(String eventInfo) {
        this.eventInfo = eventInfo;
    }
}
