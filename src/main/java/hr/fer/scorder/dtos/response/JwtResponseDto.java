package hr.fer.scorder.dtos.response;

public class JwtResponseDto {

    private String accessToken;

    private String email;

    private Long id;

    private String role;

    private String tokenType = "Bearer";

    public JwtResponseDto() {
    }

    public JwtResponseDto(final String accessToken, final String email, final Long id, final String role) {
        this.accessToken = accessToken;
        this.email = email;
        this.id = id;
        this.role = role;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(final String tokenType) {
        this.tokenType = tokenType;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(final String role) {
        this.role = role;
    }
}
