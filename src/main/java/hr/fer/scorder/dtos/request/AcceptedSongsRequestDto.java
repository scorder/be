package hr.fer.scorder.dtos.request;

public class AcceptedSongsRequestDto {

    public Long[] songIds;

    public AcceptedSongsRequestDto() {
    }

    public AcceptedSongsRequestDto(Long[] songIds) {
        this.songIds = songIds;
    }

    public Long[] getSongIds() {
        return songIds;
    }

    public void setSongIds(Long[] songIds) {
        this.songIds = songIds;
    }
}
