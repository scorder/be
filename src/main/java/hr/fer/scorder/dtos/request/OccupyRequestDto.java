package hr.fer.scorder.dtos.request;

public class OccupyRequestDto {

    private Integer tableNumber;

    public OccupyRequestDto() {
    }

    public OccupyRequestDto(final Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(final Integer tableNumber) {
        this.tableNumber = tableNumber;
    }
}
