package hr.fer.scorder.dtos.request;

public class SongRequestDto {

    private String songTitle;

    private String artistName;

    private String albumTitle;

    private String songLink;

    public SongRequestDto() {
    }

    public SongRequestDto(final String songTitle, final String artistName, final String albumTitle,
                          final String songLink) {
        this.songTitle = songTitle;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.songLink = songLink;
    }

    public String getSongTitle() {
        return this.songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getArtistName() {
        return this.artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return this.albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getSongLink() {
        return this.songLink;
    }

    public void setSongLink(String songLink) {
        this.songLink = songLink;
    }
}
