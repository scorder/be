package hr.fer.scorder.dtos.request;

public class OrderEntryRequestDto {

    private Long itemId;

    private Double quantity;

    public OrderEntryRequestDto() {
    }

    public OrderEntryRequestDto(final Long itemId, final Double quantity) {
        this.itemId = itemId;
        this.quantity = quantity;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(final Long itemId) {
        this.itemId = itemId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(final Double quantity) {
        this.quantity = quantity;
    }
}
