package hr.fer.scorder.dtos.request;

public class QuestionRequestDto {

    private String text;

    private String a;

    private String b;

    private String c;

    private String d;

    private String correct;

    private Integer numberOfQuestion;

    public QuestionRequestDto() {
    }

    public QuestionRequestDto(String text, String A, String B, String C, String D, String correct, Integer numberOfQuestion) {
        this.text = text;
        this.a = A;
        this.b = B;
        this.c = C;
        this.d = D;
        this.correct = correct;
        this.numberOfQuestion = numberOfQuestion;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public Integer getNumberOfQuestion() {
        return numberOfQuestion;
    }

    public void setNumberOfQuestion(final Integer numberOfQuestion) {
        this.numberOfQuestion = numberOfQuestion;
    }

}
