package hr.fer.scorder.dtos.request;

import java.util.List;

public class OrderRequestDto {

    private Long tableId;

    private List<OrderEntryRequestDto> orderEntries;

    public OrderRequestDto() {
    }

    public OrderRequestDto(final Long tableId, final List<OrderEntryRequestDto> orderEntries) {
        this.tableId = tableId;
        this.orderEntries = List.copyOf(orderEntries);
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(final Long tableId) {
        this.tableId = tableId;
    }

    public List<OrderEntryRequestDto> getOrderEntries() {
        return List.copyOf(orderEntries);
    }

    public void setOrderEntries(final List<OrderEntryRequestDto> orderEntries) {
        this.orderEntries = List.copyOf(orderEntries);
    }
}
