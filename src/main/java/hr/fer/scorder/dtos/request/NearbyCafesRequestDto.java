package hr.fer.scorder.dtos.request;

public class NearbyCafesRequestDto {

    private Double latitude;

    private Double longitude;

    private Double latitudeDeviation;

    private Double longitudeDeviation;

    public NearbyCafesRequestDto() {

    }

    public NearbyCafesRequestDto(final Double latitude, final Double longitude, final Double latitudeDeviation, final Double longitudeDeviation) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.latitudeDeviation = latitudeDeviation;
        this.longitudeDeviation = longitudeDeviation;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitudeDeviation() {
        return latitudeDeviation;
    }

    public void setLatitudeDeviation(Double latitudeDeviation) {
        this.latitudeDeviation = latitudeDeviation;
    }

    public Double getLongitudeDeviation() {
        return longitudeDeviation;
    }

    public void setLongitudeDeviation(Double longitudeDeviation) {
        this.longitudeDeviation = longitudeDeviation;
    }

}
