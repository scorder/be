package hr.fer.scorder.dtos.request;

public class ItemRequestDto {

    private String name;

    private String category;

    private Double price;

    private Double servingSize;

    private String unitOfMeasure;

    private Double percentDiscount;

    public ItemRequestDto() {
    }

    public ItemRequestDto(final String name, final String category, final Double price, final Double servingSize,
                          final String unitOfMeasure, final Double percentDiscount) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.servingSize = servingSize;
        this.unitOfMeasure = unitOfMeasure;
        this.percentDiscount = percentDiscount;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(final Double price) {
        this.price = price;
    }

    public Double getServingSize() {
        return servingSize;
    }

    public void setServingSize(final Double servingSize) {
        this.servingSize = servingSize;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(final String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Double getPercentDiscount() {
        return percentDiscount;
    }

    public void setPercentDiscount(final Double percentDiscount) {
        this.percentDiscount = percentDiscount;
    }
}
