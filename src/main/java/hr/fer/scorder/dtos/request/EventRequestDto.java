package hr.fer.scorder.dtos.request;

import java.time.LocalDateTime;

public class EventRequestDto {

    private String eventName;

    private LocalDateTime date;

    private LocalDateTime ending;

    private String eventInfo;

    public EventRequestDto() {
    }

    public EventRequestDto(String eventName, LocalDateTime date, LocalDateTime ending, String eventInfo) {
        this.eventName = eventName;
        this.date = date;
        this.ending = ending;
        this.eventInfo = eventInfo;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getEnding() {
        return ending;
    }

    public void setEnding(LocalDateTime ending) {
        this.ending = ending;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(String eventInfo) {
        this.eventInfo = eventInfo;
    }


}
