package hr.fer.scorder.dtos.request;

public class PlaylistRequestDto {

    private Long tableId;

    private Long[] songIds;

    public PlaylistRequestDto() {

    }

    public PlaylistRequestDto(final Long tableId, final Long[] songIds) {
        this.tableId = tableId;
        this.songIds = songIds;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    public Long[] getSongIds() {
        return songIds;
    }

    public void setSongIds(Long[] songIds) {
        this.songIds = songIds;
    }
}
