package hr.fer.scorder.dtos.request;

public class SuggestedSongRequestDto {

    private Long cafeId;

    private String songTitle;

    private String artistName;

    private String albumTitle;

    private String songLink;

    public SuggestedSongRequestDto() {
    }

    public SuggestedSongRequestDto(Long cafeId, String songTitle, String artistName, String albumTitle, String songLink) {
        this.cafeId = cafeId;
        this.songTitle = songTitle;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.songLink = songLink;
    }

    public Long getCafeId() {
        return cafeId;
    }

    public void setCafeId(Long cafeId) {
        this.cafeId = cafeId;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getSongLink() {
        return songLink;
    }

    public void setSongLink(String songLink) {
        this.songLink = songLink;
    }
}
