package hr.fer.scorder.dtos.request;

import com.sun.istack.NotNull;

public class LoginRequestDto {

    @NotNull
    private String email;

    @NotNull
    private String password;

    public LoginRequestDto() {
    }

    public LoginRequestDto(final String email, final String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
