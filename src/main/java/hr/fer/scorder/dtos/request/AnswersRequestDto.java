package hr.fer.scorder.dtos.request;

import java.util.List;

public class AnswersRequestDto {

    Long tableId;

    Long eventId;

    List<AnswerRequestDto> answers;

    public AnswersRequestDto(final Long tableId, final Long eventId, final List<AnswerRequestDto> answers) {
        this.tableId = tableId;
        this.eventId = eventId;
        this.answers = answers;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    public List<AnswerRequestDto> getAnswerRequestDtoList() {
        return answers;
    }

    public void setAnswerRequestDtoList(List<AnswerRequestDto> answerRequestDtoList) {
        this.answers = answerRequestDtoList;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(final Long eventId) {
        this.eventId = eventId;
    }

    public List<AnswerRequestDto> getAnswers() {
        return answers;
    }

    public void setAnswers(final List<AnswerRequestDto> answers) {
        this.answers = answers;
    }
}
