package hr.fer.scorder.dtos.request;

import java.time.LocalDateTime;
import java.util.List;

public class QuizRequestDto {

    private String eventName;

    private LocalDateTime date;

    private LocalDateTime ending;

    private String eventInfo;

    private List<QuestionRequestDto> questions;

    public QuizRequestDto() {
    }

    public QuizRequestDto(final String eventName, final LocalDateTime date, final LocalDateTime ending, final String eventInfo, final List<QuestionRequestDto> questions) {
        this.eventName = eventName;
        this.date = date;
        this.ending = ending;
        this.eventInfo = eventInfo;
        this.questions = questions;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(final String eventName) {
        this.eventName = eventName;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(final LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getEnding() {
        return ending;
    }

    public void setEnding(final LocalDateTime ending) {
        this.ending = ending;
    }

    public String getEventInfo() {
        return eventInfo;
    }

    public void setEventInfo(final String eventInfo) {
        this.eventInfo = eventInfo;
    }

    public List<QuestionRequestDto> getQuestions() {
        return questions;
    }

    public void setQuestions(final List<QuestionRequestDto> questions) {
        this.questions = questions;
    }
}
