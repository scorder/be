package hr.fer.scorder.dtos.request;

public class SongsToAddToSongListRequestDto {

    private String[] songsToAdd;

    private String[] songsToRemove;

    public SongsToAddToSongListRequestDto() {
    }

    public SongsToAddToSongListRequestDto(final String[] songsToAdd, final String[] songsToRemove) {
        this.songsToAdd = songsToAdd;
        this.songsToRemove = songsToRemove;
    }

    public String[] getSongsToAdd() {
        return songsToAdd;
    }

    public void setSongsToAdd(String[] songsToAdd) {
        this.songsToAdd = songsToAdd;
    }

    public String[] getSongsToRemove() {
        return songsToRemove;
    }

    public void setSongsToRemove(String[] songsToRemove) {
        this.songsToRemove = songsToRemove;
    }
}
