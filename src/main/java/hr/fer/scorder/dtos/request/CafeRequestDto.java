package hr.fer.scorder.dtos.request;

public class CafeRequestDto {

    private String name;

    private String email;

    private String password;

    private String pin;

    private Double locationX;

    private Double locationY;

    public CafeRequestDto() {
    }

    public CafeRequestDto(final String name, final String email, final String password, final String pin,
                          final Double locationX, final Double locationY) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.pin = pin;
        this.locationX = locationX;
        this.locationY = locationY;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(final String pin) {
        this.pin = pin;
    }

    public Double getLocationX() {
        return locationX;
    }

    public void setLocationX(Double locationX) {
        this.locationX = locationX;
    }

    public Double getLocationY() {
        return locationY;
    }

    public void setLocationY(Double locationY) {
        this.locationY = locationY;
    }

}
