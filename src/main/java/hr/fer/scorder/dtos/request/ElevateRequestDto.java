package hr.fer.scorder.dtos.request;

public class ElevateRequestDto {

    private String pin;

    public ElevateRequestDto() {
    }

    public ElevateRequestDto(final String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(final String pin) {
        this.pin = pin;
    }
}
