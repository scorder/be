package hr.fer.scorder.dtos.request;

public class OrderFulfillDto {

    private Long orderId;

    public OrderFulfillDto() {
    }

    public OrderFulfillDto(final Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(final Long orderId) {
        this.orderId = orderId;
    }
}
